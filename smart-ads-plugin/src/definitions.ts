declare module "@capacitor/core" {
  interface PluginRegistry {
    SmartAdsPlugin: SmartAdsPluginPlugin;
  }
}

export interface SmartAdsPluginPlugin {
  echo(options: { value: string }): Promise<{value: string}>;
}
