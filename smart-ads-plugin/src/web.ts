import { WebPlugin } from '@capacitor/core';
import { SmartAdsPluginPlugin } from './definitions';

export class SmartAdsPluginWeb extends WebPlugin implements SmartAdsPluginPlugin {
  constructor() {
    super({
      name: 'SmartAdsPlugin',
      platforms: ['web']
    });
  }
}

const SmartAdsPlugin = new SmartAdsPluginWeb();

export { SmartAdsPlugin };

import { registerWebPlugin } from '@capacitor/core';
registerWebPlugin(SmartAdsPlugin);
