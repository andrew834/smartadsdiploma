package com.smart.ads.plugin;

import com.getcapacitor.JSArray;
import com.getcapacitor.JSObject;
import com.google.android.gms.location.DetectedActivity;

import java.util.List;

public class Utils {
    public static JSObject construtcJSObject(List<DetectedActivity> detectedActivities) {
        JSObject jsObject = new JSObject();
        JSArray jsArray = new JSArray();

        for (int i = 0; i < detectedActivities.size(); i++) {
            String activityName = "UNKNOWN";
            switch (detectedActivities.get(i).getType()) {
                case DetectedActivity.IN_VEHICLE:
                    activityName = "IN_VEHICLE";
                    break;
                case DetectedActivity.ON_BICYCLE:
                    activityName = "ON_BICYCLE";
                    break;
                case DetectedActivity.ON_FOOT:
                    activityName = "ON_FOOT";
                    break;
                case DetectedActivity.RUNNING:
                    activityName = "RUNNING";
                    break;
                case DetectedActivity.STILL:
                    activityName = "STILL";
                    break;
                case DetectedActivity.TILTING:
                    activityName = "TILTING";
                    break;
                case DetectedActivity.UNKNOWN:
                    activityName = "UNKNOWN";
                    break;
                case DetectedActivity.WALKING:
                    activityName = "WALKING";
                    break;
            }

            JSObject activity = new JSObject();
            activity.put("name", activityName);
            activity.put("confidence", detectedActivities.get(i).getConfidence());
            jsArray.put(activity);
        }

        jsObject.put("activities", jsArray);

        return jsObject;
    }
}
