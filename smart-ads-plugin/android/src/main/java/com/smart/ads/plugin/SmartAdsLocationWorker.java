package com.smart.ads.plugin;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.util.Log;

import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import static android.content.Context.LOCATION_SERVICE;

public class SmartAdsLocationWorker extends Worker {

    private static final String DEBUG = "SMART_ADS_WORKER_DEBUG";

    public SmartAdsLocationWorker(
            @NonNull Context context,
            @NonNull WorkerParameters params) {
        super(context, params);
    }

    @SuppressLint("MissingPermission")
    @Override
    public Result doWork() {
        if (isGooglePlayServiceEnabled(getApplicationContext())) {
            return withFusedLocation();
        } else {
            return withLocationManager();
        }
    }

    @SuppressLint("MissingPermission")
    private Result withFusedLocation() {
        final FusedLocationProviderClient fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getApplicationContext());

        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(10000);
        locationRequest.setSmallestDisplacement(0);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationCallback locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);

                writeToFile(locationResult.getLastLocation());
                fusedLocationProviderClient.removeLocationUpdates(this);
            }
        };

        try {
            fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());
        } catch (Exception e) {
            Log.i(DEBUG, e.toString());
            return withLocationManager();
        }

        return Result.success();
    }

    @SuppressLint("MissingPermission")
    private Result withLocationManager() {
        try {
            final LocationManager mLocationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);

            String DEFAULT_PROVIDER = LocationManager.NETWORK_PROVIDER;

            if (!mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    DEFAULT_PROVIDER = LocationManager.GPS_PROVIDER;
                } else {
                    if (mLocationManager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER)) {
                        DEFAULT_PROVIDER = LocationManager.PASSIVE_PROVIDER;
                    } else {
                        return Result.failure();
                    }
                }
            }

            mLocationManager.requestLocationUpdates(DEFAULT_PROVIDER, 10000, 0, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    mLocationManager.removeUpdates(this);
                    writeToFile(location);
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {
                }

                @Override
                public void onProviderEnabled(String provider) {
                }

                @Override
                public void onProviderDisabled(String provider) {
                }
            }, Looper.getMainLooper());
        } catch (Exception e) {
            Log.i(DEBUG, e.toString());
        }

        return Result.success();
    }

    /**
     * Receives a location and writes to a file in the following csv format (separated by a ;): lat;lon;date;
     *
     * @param l - location, containing latitude and longitude of a user.
     */
    private void writeToFile(Location l) {
        try {
            File file = new File(getApplicationContext().getExternalFilesDir(null), "smart-ads.csv");

            FileWriter writer = new FileWriter(file, true);
            writer.write(l.getLatitude() + ";" + l.getLongitude() + ";" + getCurrentDate() + "\n");
            writer.flush();
            writer.close();
        } catch (Exception e) {
            Log.e(DEBUG, "Write to file failed: " + e.toString());
        }
    }

    /**
     * Converts the contents of a file into a string value.
     *
     * @return - A string value representing either an exception or the content of the smart-ads.csv file.
     */
    private String fileToString() {
        try {
            File file = new File(getApplicationContext().getExternalFilesDir(null), "smart-ads.csv");

            if (!file.exists()) {
                return "fileToString ~ No such file.";
            }

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
            StringBuilder stringBuilder = new StringBuilder();
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line).append("\n");
            }

            bufferedReader.close();

            return stringBuilder.toString();
        } catch (Exception e) {
            return e.toString();
        }
    }

    /**
     * Returns the current date in an ISO format following the pattern: "yyyy-MM-dd'T'HH:mm'Z'".
     *
     * @return - A string value representing the current date.
     */
    private static String getCurrentDate() {
        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
        dateFormat.setTimeZone(timeZone);

        return dateFormat.format(new Date());
    }

    /**
     * The function returns a boolean value that tells if google play services is enabled or not.
     *
     * @param context - Application context
     * @return - A boolean value representing the availability of google play services.
     */
    private boolean isGooglePlayServiceEnabled(Context context) {
        GoogleApiAvailability mGAA = GoogleApiAvailability.getInstance();
        int resultCode = mGAA.isGooglePlayServicesAvailable(context);

        return resultCode == ConnectionResult.SUCCESS;
    }
}