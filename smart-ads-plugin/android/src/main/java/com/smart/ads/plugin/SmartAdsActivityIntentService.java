package com.smart.ads.plugin;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

public class SmartAdsActivityIntentService extends IntentService {
    protected static final String TAG = "SMART_ADS_ACTIVITY_INTENT_SERVICE";

    public SmartAdsActivityIntentService() {
        super(TAG);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, startId, startId);
        return START_STICKY;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        intent.setAction(Constants.SMART_ADS_DETECTED_ACTIVITIES);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }
}
