package com.smart.ads.plugin;

public class Constants {
    static final String SMART_ADS_DETECTED_ACTIVITIES = "SMART_ADS_DETECTED_ACTIVITIES";
    static final String SMART_ADS_JOB_TAG = "smart-ads-location-sampling";
    static final String SMART_ADS_JAVASCRIPT_EVENT_SUCCESS = "updateUserActivities";
    static final String SMART_ADS_JAVASCRIPT_EVENT_FAILURE = "updateUserActivitiesError";
}