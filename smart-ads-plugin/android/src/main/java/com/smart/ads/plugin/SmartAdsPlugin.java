package com.smart.ads.plugin;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.getcapacitor.JSObject;
import com.getcapacitor.NativePlugin;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;

import com.google.android.gms.location.ActivityRecognitionClient;
import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

import java.util.List;
import java.util.concurrent.TimeUnit;

@NativePlugin()
public class SmartAdsPlugin extends Plugin {

    private static final String DEBUG = "SMART_ADS_WORKER_DEBUG";
    private static final int DETECTION_INTERVAL_IN_MILLISECONDS_DEFAULT = 10000;
    private boolean isPluginEnabled = false;
    private BroadcastReceiver mBroadcastReceiver;
    private ActivityRecognitionClient mActivityRecognitionClient;
    private WorkManager mWorkManager;

    @PluginMethod()
    public void enableActivityUpdates(PluginCall call) {
        if (isPluginEnabled) {
            return;
        }

        Integer duration = call.getInt("duration");
        isPluginEnabled = true;
        mActivityRecognitionClient = new ActivityRecognitionClient(getContext());

        mActivityRecognitionClient.requestActivityUpdates(
                (duration == null) ? DETECTION_INTERVAL_IN_MILLISECONDS_DEFAULT : duration,
                getActivityDetectionPendingIntent());

        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {
                    ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);
                    List<DetectedActivity> detectedActivities = result.getProbableActivities();
                    JSObject returnResult = Utils.construtcJSObject(detectedActivities);
                    bridge.triggerDocumentJSEvent(Constants.SMART_ADS_JAVASCRIPT_EVENT_SUCCESS, returnResult.toString());
                } catch (Exception e) {
                    bridge.triggerDocumentJSEvent(Constants.SMART_ADS_JAVASCRIPT_EVENT_FAILURE, e.toString());
                }
            }
        };

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.SMART_ADS_DETECTED_ACTIVITIES);

        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mBroadcastReceiver, intentFilter);

        call.success();
    }

    @PluginMethod()
    public void disableActivityUpdates(PluginCall call) {
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mBroadcastReceiver);
        mBroadcastReceiver = null;

        mActivityRecognitionClient.removeActivityUpdates(disableActivityDetectionPendingIntent());
        mActivityRecognitionClient = null;

        isPluginEnabled = false;

        call.success();
    }

    private PendingIntent getActivityDetectionPendingIntent() {
        Intent intent = new Intent(getContext(), SmartAdsActivityIntentService.class);

        return PendingIntent.getService(getContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private PendingIntent disableActivityDetectionPendingIntent() {
        Intent intent = new Intent(getContext(), SmartAdsActivityIntentService.class);

        return PendingIntent.getService(getContext(), 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
    }

    @PluginMethod()
    public void getBrightness(PluginCall call) {
        try {
            float brightnessValue = android.provider.Settings.System.getInt(
                    getContext().getContentResolver(), android.provider.Settings.System.SCREEN_BRIGHTNESS);

            JSObject obj = new JSObject();
            obj.put("brightness", brightnessValue);
            call.resolve(obj);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
    }

    @PluginMethod()
    public void enableLocationManager(PluginCall call) {
        Log.i(DEBUG, "enableLocationManager");
        if (mWorkManager == null) {
            mWorkManager = WorkManager.getInstance();
        }

        PeriodicWorkRequest.Builder workBuilder =
                new PeriodicWorkRequest.Builder(SmartAdsLocationWorker.class, 20, TimeUnit.MINUTES);

        mWorkManager.enqueueUniquePeriodicWork(Constants.SMART_ADS_JOB_TAG, ExistingPeriodicWorkPolicy.REPLACE, workBuilder.build());

        call.success();
    }

    @PluginMethod()
    public void disableLocationManager(PluginCall call) {
        Log.i(DEBUG, "disableLocationManager");
        if (mWorkManager == null) {
            mWorkManager = WorkManager.getInstance();
            mWorkManager.cancelAllWorkByTag(Constants.SMART_ADS_JOB_TAG);
        }

        call.success();
    }
}
