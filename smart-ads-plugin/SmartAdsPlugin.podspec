
  Pod::Spec.new do |s|
    s.name = 'SmartAdsPlugin'
    s.version = '0.0.1'
    s.summary = 'All needed plugins for SmartAds.'
    s.license = 'MIT'
    s.homepage = 'none'
    s.author = 'Andrej Martinovič'
    s.source = { :git => 'none', :tag => s.version.to_s }
    s.source_files = 'ios/Plugin/**/*.{swift,h,m,c,cc,mm,cpp}'
    s.ios.deployment_target  = '11.0'
    s.dependency 'Capacitor'
  end