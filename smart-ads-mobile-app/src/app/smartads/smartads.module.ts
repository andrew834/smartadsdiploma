import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';

import { SmartAdsRoutingModule } from './smartads-routing.module';
import { SmartAdsPage } from './smartads.page';

@NgModule({
	imports: [ CommonModule, IonicModule, SmartAdsRoutingModule ],
	declarations: [ SmartAdsPage ]
})
export class SmartAdsPageModule {}
