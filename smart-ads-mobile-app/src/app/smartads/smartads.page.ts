import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
	templateUrl: './smartads.page.html',
	styleUrls: [ './smartads.page.scss' ]
})
export class SmartAdsPage implements OnInit {
	constructor(private menuCtrl: MenuController) {}

	ngOnInit() {}

	ionViewDidEnter() {
		this.menuCtrl.swipeGesture(true);
	}
}
