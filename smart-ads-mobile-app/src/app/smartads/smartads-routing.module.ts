import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SmartAdsPage } from './smartads.page';

const routes: Routes = [
	{
		path: 'tabs',
		component: SmartAdsPage,
		children: [
			{
				path: 'news',
				loadChildren: './news/news.module#NewsPageModule'
			},
			{
				path: 'lpp',
				loadChildren: './lpp/lpp.module#LppPageModule'
			},
			{
				path: 'restaurant',
				loadChildren: './restaurant/restaurant.module#RestaurantPageModule'
			},
			{
				path: '',
				redirectTo: '/smartads/tabs/home',
				pathMatch: 'full'
			}
		]
	},
	{
		path: 'profile',
		loadChildren: './profile/profile.module#ProfilePageModule'
	},
	{
		path: 'task',
		loadChildren: './task/task.module#TaskPageModule'
	},
	{
		path: 'train',
		loadChildren: './train/train.module#TrainPageModule'
	},
	{
		path: 'timetable',
		loadChildren: './timetable/timetable.module#TimetablePageModule'
	},
	{
		path: 'tools',
		loadChildren: './tools/tools.module#ToolsPageModule'
	},
	{
		path: 'notes',
		loadChildren: './notes/notes.module#NotesPageModule'
	}
];

@NgModule({
	imports: [ RouterModule.forChild(routes) ],
	exports: [ RouterModule ]
})
export class SmartAdsRoutingModule {}
