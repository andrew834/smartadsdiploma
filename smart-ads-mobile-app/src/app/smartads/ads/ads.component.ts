import { Component, OnInit, ViewChild, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { HttpService } from 'src/app/shared/services/http.service';
import { Plugins } from '@capacitor/core';
import { NavController, Platform, ModalController } from '@ionic/angular';
import { FeedbackComponent } from './feedback/feedback.component';
import { ContextService } from 'src/app/shared/services/context.service';
import { take } from 'rxjs/operators';
import { Subscription } from 'rxjs';

const { StatusBar, Storage } = Plugins;

@Component({
	templateUrl: './ads.component.html',
	styleUrls: [ './ads.component.scss' ]
})
export class AdsComponent implements OnInit, OnDestroy {
	videoTimeLeft = null;
	currentVideoUrl = null;

	currentImageUrl = 'assets/img/black.jpg';
	currentIndex = 0;
	videoInterval = null;

	soundIcon = 'volume-high';
	currentMode;
	videoType = null;

	adStarted = null;

	@ViewChild('videoEl', null)
	videoEl;

	reader = new FileReader();

	serverId = null;
	survey = null;

	constructor(
		private httpService: HttpService,
		private changeDetector: ChangeDetectorRef,
		private navCtrl: NavController,
		private platform: Platform,
		private modalCtrl: ModalController,
		private contextService: ContextService
	) {}

		backButtonSub: Subscription;

	ngOnInit() {
		Storage.get({key: "smart-ads-locations-labled"}).then(storedServerData => {
			if(storedServerData.value != null){
				this.httpService.sendSemanticLocation(JSON.parse(storedServerData.value)).subscribe(serverRes => {
					Storage.remove({key: "smart-ads-locations-labled"});
				});
			}
		});

		StatusBar.hide();

		this.currentMode = Math.random() <= 0.33 ? 'image' : 'video';
		if (this.currentMode === 'video') {
			this.videoType = Math.random() <= 0.5 ? 'short' : 'long';
			this.httpService.getRandomVideo(this.videoType).subscribe((serverRes: any) => {
				this.backButtonSub = this.platform.backButton.subscribeWithPriority(9999, () => {
					// nothing
				});

				screen.orientation.lock('landscape').finally(() => {
					this.currentVideoUrl = serverRes.url;
					this.survey = serverRes.survey;
					console.log(this.survey);
					this.enableVideoTimer();
					this.changeDetector.detectChanges();
				});
			});
		} else {
			this.httpService.getRandomImage().subscribe(
				(serverRes: any) => {
					this.backButtonSub = this.platform.backButton.subscribeWithPriority(9999, () => {
						// nothing
					});
					
					this.currentImageUrl = serverRes.url;
					this.survey = serverRes.survey;
					console.log(this.survey);
					this.adStarted = new Date();
				},
				(serverErr) => {
					console.log(serverErr);
				}
			);
		}
	}

	onChangeSound() {
		if (this.soundIcon === 'volume-high') {
			this.soundIcon = 'volume-mute';
			(document.getElementById('video-ad') as HTMLVideoElement).muted = true;
		} else {
			this.soundIcon = 'volume-high';
			(document.getElementById('video-ad') as HTMLVideoElement).muted = false;
		}
	}

	forceClose() {
		this.close();
	}

	enableVideoTimer() {
		if (this.videoInterval) {
			clearInterval(this.videoInterval);
			this.videoTimeLeft = null;
		}

		this.adStarted = new Date();

		this.videoInterval = setInterval(() => {
			if (this.videoTimeLeft != null && this.videoTimeLeft <= 0 && this.videoInterval) {
				clearInterval(this.videoInterval);
				this.close();
				return;
			}

			if ((document.getElementById('video-ad') as HTMLVideoElement) == null) {
				clearInterval(this.videoInterval);
				return;
			}

			const videoDuration = (document.getElementById('video-ad') as HTMLVideoElement).duration;
			this.videoTimeLeft = Math.ceil(
				videoDuration - (document.getElementById('video-ad') as HTMLVideoElement).currentTime
			);
		}, 500);
	}

	close() {
		const mode = this.currentMode;
		const lengthWatched = (new Date().getTime() - this.adStarted.getTime()) / 1000; // v sekundah
		// Poslji podatke na streznik, koliko casa je user prezivel na oglasu + context

		console.log(lengthWatched);
		this.contextService.context.pipe(take(1)).subscribe((data) => {
			console.log(this.survey);
			// ce je data.userLocation.currentlocation.home.lat == 0 potem dbscan se ni narjen in mormo dt v Storage
			const sendObj = {
				name: this.survey.n,
				category: mode == 'image' ? 'image' : this.videoType == 'short' ? 'vshort' : 'vlong',
				duration: lengthWatched,
				activity: data.activity,
				connection: data.connection.connectionType,
				battery: {
					level: data.battery.level,
					isPlugged: data.battery.isPlugged
				},
				brightness: data.brightness.value,
				time: data.time.date,
				location:
					!Array.isArray(data.userLocation.home) && data.userLocation.home.lat == 0
						? 'UNKNOWN'
						: data.userLocation.currentLocation.status
			};

			this.httpService.sendData(sendObj).subscribe((response) => {
				this.serverId = response.id;
				if (!Array.isArray(data.userLocation.home) && data.userLocation.home.lat == 0) {
					Storage.get({ key: 'smart-ads-locations-cache' }).then((storedData) => {
						let storageData = [];
						if (storedData.value != null) {
							storageData = JSON.parse(storedData.value);
						}

						storageData.push({
							id: response.id,
							location: {
								lat: data.userLocation.currentLocation.lat,
								lng: data.userLocation.currentLocation.lng
							}
						});

						console.log(storageData);

						Storage.set({ key: 'smart-ads-locations-cache', value: JSON.stringify(storageData) });
					});
				}

				this.currentMode = null;

				screen.orientation.lock('portrait').then(() => {
					if (this.videoInterval) {
						(document.getElementById('video-ad') as HTMLVideoElement).pause();
					}

					setTimeout(() => {
						Storage.get({ key: 'smart-ads-feedback' }).then((storedData) => {
							this.modalCtrl.dismiss(null);
							if (storedData.value != null) {
								let feedbackData = JSON.parse(storedData.value);
								if (new Date(feedbackData.globalReset).getDate() != new Date().getDate()) {
									this.updateStorage(mode);

									setTimeout(() => {
										this.modalCtrl
											.create({
												backdropDismiss: false,
												component: FeedbackComponent,
												cssClass: 'feedback',
												componentProps: {
													id: response.id,
													survey: this.survey
												}
											})
											.then((modalEl) => {
												modalEl.present();
											});
									}, 1000);

									return;
								}

								if (feedbackData.totalFeedback < feedbackData.maxFeedback) {
									if (
										!(
											mode == 'video' &&
											(feedbackData.videoFeedback == 2 ||
												new Date(feedbackData.videoTimer).getTime() > new Date().getTime())
										) &&
										!(
											mode == 'image' &&
											(feedbackData.imageFeedback == 3 ||
												new Date(feedbackData.imageTimer).getTime() > new Date().getTime())
										)
									) {
										feedbackData.totalFeedback += 1;
										feedbackData.videoFeedback += mode == 'video' ? 1 : 0;
										feedbackData.imageFeedback += mode == 'image' ? 1 : 0;
										feedbackData.imageTimer =
											mode == 'image'
												? new Date(new Date().getTime() + 1000 * 60 * 60 * 3)
												: feedbackData.imageTimer;
										feedbackData.videoTimer =
											mode == 'video'
												? new Date(new Date().getTime() + 1000 * 60 * 60 * 3)
												: feedbackData.videoTimer;

										this.updateStorage(mode, feedbackData);

										setTimeout(() => {
											this.modalCtrl
												.create({
													backdropDismiss: false,
													component: FeedbackComponent,
													cssClass: 'feedback',
													componentProps: {
														id: response.id,
														survey: this.survey
													}
												})
												.then((modalEl) => {
													modalEl.present();
												});
										}, 1000);
									}
								}
							} else {
								this.updateStorage(mode);

								setTimeout(() => {
									this.modalCtrl
										.create({
											backdropDismiss: false,
											component: FeedbackComponent,
											cssClass: 'feedback',
											componentProps: {
												id: response.id,
												survey: this.survey
											}
										})
										.then((modalEl) => {
											modalEl.present();
										});
								}, 1000);
							}
						});
					}, 200);
				});
			});
		});
	}

	updateStorage(mode, newData?) {
		let data = {
			globalReset: new Date(),
			totalFeedback: 1,
			maxFeedback: 5,
			videoFeedback: mode == 'video' ? 1 : 0,
			imageFeedback: mode == 'image' ? 1 : 0,
			imageTimer: mode == 'image' ? new Date(new Date().getTime() + 1000 * 60 * 60 * 3) : new Date(),
			videoTimer: mode == 'video' ? new Date(new Date().getTime() + 1000 * 60 * 60 * 3) : new Date()
		};

		if (newData != null) {
			data = newData;
		}

		return Storage.set({ key: 'smart-ads-feedback', value: JSON.stringify(data) });
	}

	ngOnDestroy() {
		if(this.backButtonSub) {
			this.backButtonSub.unsubscribe();
		}

		if (this.videoInterval) {
			clearInterval(this.videoInterval);
		}
	}
}
