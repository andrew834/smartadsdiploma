import { Component, OnInit, Input } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { HttpService } from 'src/app/shared/services/http.service';

@Component({
	templateUrl: './feedback.component.html',
	styleUrls: [ './feedback.component.scss' ]
})
export class FeedbackComponent {
	@Input('id') id;
	@Input('survey') survey;

	constructor(private alertCtrl: AlertController, private modalCtrl: ModalController, private httpService: HttpService) {}

	selectedOne = null;
	selectedTwo = null;
	selectedThree = null;

	onError(header: string, message: string) {
		this.alertCtrl
			.create({
				header,
				message,
				buttons: [ 'Zapri' ]
			})
			.then((alert) => {
				alert.present();
			});
	}

	sendData() {
		this.httpService.sendFeedback({
			a1: this.selectedOne,
			a2: this.selectedTwo,
			a3: this.selectedThree, 
			id: this.id
		}).subscribe(() => {
			this.modalCtrl.dismiss(null);
		}, err => {
			console.log(err);
			this.modalCtrl.dismiss(null);
		});
	}

	clickedRadio(event, index) {
		if (index == '0') {
			this.selectedOne = event.detail.value;
		} else if (index == '1') {
			this.selectedTwo = event.detail.value;
		} else if (index == '2') {
			this.selectedThree = event.detail.value;
		}
	}
}
