import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { LppPage } from './lpp.page';
import {
	MatInputModule,
	MatCardModule,
	MatButtonModule,
	MatToolbarModule,
	MatExpansionModule,
	MatProgressSpinnerModule,
	MatPaginatorModule,
	MatDialogModule,
	MatOptionModule,
	MatSelectModule,
	MatDividerModule,
	MatCheckboxModule,
	MatSlideToggleModule,
	MatTableModule
} from '@angular/material';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CdkTableModule } from '@angular/cdk/table';

const routes: Routes = [
	{
		path: '',
		component: LppPage
	}
];

@NgModule({
	imports: [
		CommonModule,
		IonicModule,
		RouterModule.forChild(routes),
		MatInputModule,
		MatCardModule,
		MatButtonModule,
		MatFormFieldModule,
		MatToolbarModule,
		MatTableModule,
		MatExpansionModule,
		CdkTableModule,
		MatProgressSpinnerModule,
		MatPaginatorModule,
		MatDialogModule,
		MatOptionModule,
		MatSelectModule,
		MatDividerModule,
		MatCheckboxModule,
		MatSlideToggleModule,
		MatAutocompleteModule,
		FormsModule,
		ReactiveFormsModule
	],
	declarations: [ LppPage ]
})
export class LppPageModule {
	constructor() {}
}
