import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpService } from 'src/app/shared/services/http.service';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { DataSource } from '@angular/cdk/table';
import { ActionsService } from 'src/app/shared/services/actions.service';

@Component({
	templateUrl: './lpp.page.html',
	styleUrls: [ './lpp.page.scss' ]
})
export class LppPage implements OnInit {
	@ViewChild('busSearch', { static: false })
	busSearchEl;
	stationSearchControl = new FormControl();
	currentMode: 'toCenter' | 'fromCenter' = 'toCenter';
	toCenter: string[] = [];
	toCenterHm;

	fromCenter: string[] = [];
	fromCenterHm;
	filteredOptions: Observable<string[]>;
	selectedOption = 'toCenter';

	displayedColumns: string[] = [ 'linija', 'ime', 'prihodi' ];
	dataSource = [];

	constructor(private httpService: HttpService, private actionsService: ActionsService) {}

	ngOnInit() {
		this.httpService.getBusesToCenter().subscribe((serverRes) => {
			this.toCenterHm = serverRes.l;
			this.toCenter = this.hashmapToArray(serverRes.l);
		});

		this.httpService.getBusesFromCenter().subscribe((serverRes) => {
			this.fromCenterHm = serverRes.l;
			this.fromCenter = this.hashmapToArray(serverRes.l);
		});

		this.filteredOptions = this.stationSearchControl.valueChanges.pipe(
			startWith(''),
			map((value) => this._filter(value))
		);
	}

	private _filter(value: string): string[] {
		const filterValue = value.toLowerCase();

		if (this.selectedOption === 'toCenter') {
			return this.toCenter.filter((station) => station.toLowerCase().indexOf(filterValue) === 0);
		} else {
			return this.fromCenter.filter((station) => station.toLowerCase().indexOf(filterValue) === 0);
		}
	}

	hashmapToArray(hashmap) {
		const array = [];
		let i = 0;

		for (const key in hashmap) {
			array[i] = `${hashmap[key]} (${key})`;
			i++;
		}

		return array;
	}

	onSelectionChange() {
		this.selectedOption = this.selectedOption === 'toCenter' ? 'fromCenter' : 'toCenter';
		this.busSearchEl.nativeElement.value = '';
		this.stationSearchControl.reset('');
	}

	getBusData(event, option) {
		try {
			const stationId = option.substring(option.indexOf('(') + 1, option.indexOf(')'));
			this.httpService.getSpecificStationInformation(stationId).subscribe((serverRes) => {
				const data = [];

				for (let i = 0; i < serverRes.l.length; i++) {
					if (serverRes.l[i].length === 0) {
						continue;
					}

					const newInfo = Object.create(null);

					for (let j = 0; j < serverRes.l[i].length; j++) {
						const busInfo = serverRes.l[i][j];
						newInfo.linija = busInfo.key;
						newInfo.ime = busInfo.name;
						if (j === 0) {
							newInfo.prihodi = `${busInfo.minutes} min`;
						} else {
							newInfo.prihodi += `, ${busInfo.minutes} min`;
						}
					}

					data.push(newInfo);
				}

				this.dataSource = data;
				setTimeout(() => {
					document.getElementById("busSearchField").blur();
					this.actionsService.actionFiredShowAd();
				}, 100);
			});
		} catch (e) {
			console.log(e);
		}
	}
}
