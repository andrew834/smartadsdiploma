import { Component, OnInit } from '@angular/core';
import {
	AlertController,
	ModalController,
	LoadingController,
	PopoverController
} from '@ionic/angular';
import { CreateNoteComponent } from './create/create.component';
import { PopoverNotesComponent } from './popover/popover.component';
import { trigger, style, animate, transition } from '@angular/animations';
import { Plugins, FilesystemDirectory, FilesystemEncoding } from '@capacitor/core';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { ViewNoteComponent } from './view-image/view.component';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { ActionsService } from 'src/app/shared/services/actions.service';

const { Storage, Filesystem } = Plugins;

@Component({
	templateUrl: './notes.page.html',
	styleUrls: [ './notes.page.scss' ],
	animations: [
		trigger('enter', [
			transition(':enter', [
				style({ transform: 'translateX(-100%)' }),
				animate('70ms', style({ transform: 'translateX(0)' }))
			]),
			transition(':leave', [
				style({ transform: 'translateX(0)'}),
				animate('70ms', style({ transform: 'translateX(-100%)'}))
			])
		])
	]
})
export class NotesPage implements OnInit {
	isEditingEnabled = false;
	currentDate = new Date();

	constructor(
		private modalCtrl: ModalController,
		private alertCtrl: AlertController,
		private loadingCtrl: LoadingController,
		private popoverController: PopoverController,
		private fileOpener: FileOpener,
		private socialSharing: SocialSharing,
		private actionsService: ActionsService
	) {}

	currentMode = 'view';
	currentModeIcon = 'school';

	notes = [];

	deleteMode() {
		this.currentMode = 'delete';
		this.currentModeIcon = 'checkmark';
	}

	selectOrFinishMode(event) {
		if (this.currentMode != 'view') {
			event.stopPropagation();

			if (this.anyItemsToDelete()) {
				this.alertCtrl
					.create({
						header: 'Brisanje zapiskov',
						message: 'Ali ste prepričani, da želite izbrisati izbrane zapiske?',
						buttons: [
							{
								text: 'Prekliči'
							},
							{
								text: 'Da',
								handler: () => {
									this.loadingCtrl
										.create({
											message: 'Brisanje zapiskov...',
											duration: 250
										})
										.then((loader) => {
											loader.present();
											this.deleteSelected();
											return loader.onDidDismiss().then(_ => {
												this.actionsService.actionFiredShowAd();
											});
										});
								}
							}
						]
					})
					.then((alertEl) => {
						alertEl.present();
						return alertEl.onDidDismiss().then((_) => {
							this.resetDeleteQueue();
							this.currentMode = 'view';
							this.currentModeIcon = 'school';
						});
					});
			} else {
				this.currentMode = 'view';
				this.currentModeIcon = 'school';
			}
		}
	}

	deleteSelected() {
		let newNotes = [];
		for (let i = 0; i < this.notes.length; i++) {
			if (!this.notes[i].deleteQueue) {
				newNotes.push({
					name: this.notes[i].name,
					deleteQueue: false,
					courseNotes: []
				});
			} else {
				continue;
			}

			for (let j = 0; j < this.notes[i].courseNotes.length; j++) {
				if (!this.notes[i].courseNotes[j].deleteQueue) {
					newNotes[newNotes.length - 1].courseNotes.push(Object.assign({}, this.notes[i].courseNotes[j]));
				}
			}
		}

		let saveObj = JSON.parse(JSON.stringify(newNotes));

		for(let i = 0; i < saveObj.length; i++){
			for(let j = 0; j < saveObj[i].courseNotes.length; j++){
				saveObj[i].courseNotes[j].src = null;
			}
		}

		Storage.set({key: "smart-ads-zapiski", value: JSON.stringify(saveObj)}).then(_ => {
			this.notes = newNotes;
		});
	}

	anyItemsToDelete() {
		for (let i = 0; i < this.notes.length; i++) {
			if (this.notes[i].deleteQueue) {
				return true;
			}

			for (let j = 0; j < this.notes[i].courseNotes.length; j++) {
				if (this.notes[i].courseNotes[j].deleteQueue) {
					return true;
				}
			}
		}

		return false;
	}

	resetDeleteQueue() {
		for (let i = 0; i < this.notes.length; i++) {
			this.notes[i].deleteQueue = false;
			for (let j = 0; j < this.notes[i].courseNotes.length; j++) {
				this.notes[i].courseNotes[j].deleteQueue = false;
			}
		}
	}

	openPopover() {
		this.popoverController
			.create({
				component: PopoverNotesComponent,
				translucent: false,
				event
			})
			.then((popoverEl) => {
				popoverEl.present();
				return popoverEl.onDidDismiss().then((returnData) => {
					if (returnData.data != null) {
						if (returnData.data.e == 'courseNote') {
							this.newCourseNote();
						} else {
							this.alertCtrl
								.create({
									header: 'Dodaj novo skupino',
									message: 'Ustvarjanje novih skupin vam omogoča natančno organiziranje zapiskov.',
									inputs: [
										{
											name: 'group',
											placeholder: 'Naziv skupine'
										}
									],
									buttons: [
										{
											text: 'Prekliči'
										},
										{
											text: 'Shrani',
											handler: (data) => {
												if (data.group != null && data.group.length != 0) {
													this.notes.push({
														name: data.group,
														deleteQueue: false,
														courseNotes: []
													});
													Storage.set({key: "smart-ads-zapiski", value: JSON.stringify(this.notes)});
													
													this.actionsService.actionFiredShowAd();
												}
											}
										}
									]
								})
								.then((alertEl) => {
									alertEl.present();
								});
						}
					}
				});
			});
	}

	newCourseNote() {
		if (this.notes.length == 0) {
			return this.alertCtrl
				.create({
					header: 'Oh ne!',
					message: 'Za nov zapisek je potrebno ustvariti vsaj eno skupino.',
					buttons: [ 'Razumem' ]
				})
				.then((alertEl) => {
					alertEl.present();
				});
		}

		this.modalCtrl
			.create({
				component: CreateNoteComponent,
				componentProps: {
					notes: this.notes
				}
			})
			.then((modalEl) => {
				modalEl.present();
				return modalEl.onDidDismiss().then((returnData) => {
					if (returnData.data != null) {
						this.notes = returnData.data.notes;
						this.actionsService.actionFiredShowAd();
					}
				});
			});
	}

	ngOnInit() {
		Storage.get({key: "smart-ads-zapiski"}).then(data => {
			const jsonData = JSON.parse(data.value);
			if(jsonData != null){
				this.notes = jsonData;
				this.getAllImages().then(results => {
					for (let i = 0; i < results.length; i++) {
						this.notes[results[i].noteIndex].courseNotes[results[i].courseIndex].src = results[i].image;
					}
				}).catch(err => {
					this.onError("Prišlo je do napake pri branju datotek.");
				});
			}
		});
	}

	getAllImages() {
		let allImages = [];

		for(let i = 0; i < this.notes.length; i++) {
			for(let j = 0; j < this.notes[i].courseNotes.length; j++){
				if(this.notes[i].courseNotes[j].type == 'pic'){
					this.notes[i].courseNotes[j].src = "assets/img/img-placeholder.png";
					const path = this.notes[i].courseNotes[j].path;
					allImages.push(this.getImage(path, i, j));
				} else {
					this.notes[i].courseNotes[j].src = "assets/img/pdf.png";
				}
			}
		}

		return Promise.all(allImages);
	}

	getImage(filePath, i, j){
		return Filesystem.readFile({
			path: filePath,
			directory: FilesystemDirectory.External,
			encoding: FilesystemEncoding.UTF8
		  }).then(image => {
			  return {
				  noteIndex: i,
				  courseIndex: j,
				  image: image.data
			  }
		  });
	}

	openFile(courseNote) {
		if(courseNote.type == 'pic'){
			this.modalCtrl.create({
				component: ViewNoteComponent,
				componentProps: {
					image: courseNote.src
				}
			}).then(modalEl => {
				modalEl.present();
			}).catch(_ => {
				this.onError("Datoteke ni bilo mogoče odpreti.");
			});
		} else {
			Filesystem.getUri({
				directory: FilesystemDirectory.External,
				path: courseNote.path
			}).then(result => {
				this.fileOpener.open(result.uri, 'application/pdf').catch(_ => {
					this.onError("Datoteke ni bilo mogoče odpreti.");
				});
			});
		}
	}

	shareFile(courseNote) {
		if(courseNote.type === 'pic'){
			this.socialSharing.share(courseNote.description, courseNote.title, courseNote.src)
		} else {
			Filesystem.getUri({
				directory: FilesystemDirectory.External,
				path: courseNote.path
			}).then(result => {
				this.fileOpener.open(result.uri, 'application/pdf').catch(_ => {
					this.onError("Datoteke ni bilo mogoče odpreti.");
				});
			});
		}
	}

	queueCourseNoteForDelete(event, noteIndex, courseNoteIndex) {
		this.notes[noteIndex].courseNotes[courseNoteIndex].deleteQueue = event.detail.checked;
	}

	queueNoteForDelete(event, noteIndex) {
		event.stopPropagation();
		this.notes[noteIndex].deleteQueue = event.detail.checked;
		for (let i = 0; i < this.notes[noteIndex].courseNotes.length; i++) {
			this.notes[noteIndex].courseNotes[i].deleteQueue = event.detail.checked;
		}
	}

	onError(errMessage) {
		this.alertCtrl.create({
			header: "Oh ne!",
			message: errMessage,
			buttons: ["Razumem"]
		}).then(alertEl => {
			alertEl.present();
		});
	}
}
