import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { NotesPage } from './notes.page';
import { MatExpansionModule, MatInputModule } from '@angular/material';
import { CreateNoteComponent } from './create/create.component';
import { PopoverNotesComponent } from './popover/popover.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { ViewNoteComponent } from './view-image/view.component';
import { PinchZoomModule } from 'ngx-pinch-zoom';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

const routes: Routes = [
	{
		path: '',
		component: NotesPage
	}
];

@NgModule({
	imports: [
		PinchZoomModule,
		CommonModule,
		IonicModule,
		MatExpansionModule,
		MatInputModule,
		RouterModule.forChild(routes),
		ReactiveFormsModule,
		FormsModule
	],
	declarations: [ NotesPage, CreateNoteComponent, PopoverNotesComponent, ViewNoteComponent ],
	entryComponents: [ CreateNoteComponent, PopoverNotesComponent, ViewNoteComponent ],
	providers: [ FileChooser, FileOpener, SocialSharing ]
})
export class NotesPageModule {
	constructor() {}
}
