import { Component } from '@angular/core';
import { PopoverController, NavParams } from '@ionic/angular';

@Component({
	templateUrl: './popover.component.html',
	styleUrls: [ './popover.component.scss' ]
})
export class PopoverNotesComponent {
	constructor(private popoverCtrl: PopoverController) {}

	selectedOption(option) {
		this.popoverCtrl.dismiss({ e: option });
	}
}
