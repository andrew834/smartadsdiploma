import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';

import { AlertController, ModalController, ActionSheetController } from '@ionic/angular';

import { Plugins, CameraResultType, FilesystemDirectory, FilesystemEncoding } from '@capacitor/core';

import { v4 as uuid } from 'uuid';
import { FileChooser } from '@ionic-native/file-chooser/ngx';

const { Camera, Filesystem, Storage } = Plugins;

@Component({
	templateUrl: './create.component.html',
	styleUrls: [ './create.component.scss' ]
})
export class CreateNoteComponent implements OnInit {
	@Input() notes;
	form: FormGroup;

	constructor(
		private alertCtrl: AlertController,
		private modalCtrl: ModalController,
		private actionSheetController: ActionSheetController,
		private fileChooser: FileChooser
	) {}

	ngOnInit() {
		this.form = new FormGroup({
			title: new FormControl(null, {
				updateOn: 'change',
				validators: [ Validators.required, Validators.minLength(3), Validators.maxLength(40) ]
			}),
			description: new FormControl(null, {
				updateOn: 'change',
				validators: [ Validators.required, Validators.minLength(0), Validators.maxLength(2000) ]
			}),
			noteGroup: new FormControl(null, {
				updateOn: 'change',
				validators: [ Validators.required ]
			})
		});
	}

	onSelectFile() {
		if (!this.form.valid) {
			this.onError('Oh ne!', 'Nekatera polja so neustrezno izpolnjena.');
			return;
		}

		this.actionSheetController
			.create({
				header: 'Vrsta datoteke',
				buttons: [
					{
						text: 'PDF Dokument',
						icon: 'document',
						handler: () => {
							const mimeType = { mime: 'application/pdf' };
							this.fileChooser
								.open(mimeType)
								.then((result) => {
									const id = uuid();
									Filesystem.readFile({
										path: result
									})
										.then((returnData) => {
											Filesystem.writeFile({
												path: `smart-ads-files/${id}.pdf`,
												data: returnData.data,
												directory: FilesystemDirectory.External
											})
												.then((_) => {
													this.updateStoragePdf(`smart-ads-files/${id}.pdf`, this.form.value);
												})
												.catch((err) => {
													this.onError('Napaka', 'Prišlo je do napake pri branju datoteke.');
												});
										})
										.catch((err) => {
											this.onError('Napaka', 'Prišlo je do napake pri branju datoteke.');
										});
								})
								.catch((err) => {
									this.onError('Napaka', 'Prišlo je do napake pri branju datoteke.');
								});
						}
					},
					{
						text: 'Slika',
						icon: 'photos',
						handler: () => {
							Camera.getPhoto({
								quality: 45,
								allowEditing: false,
								resultType: CameraResultType.DataUrl,
								correctOrientation: true
							})
								.then((image) => {
									const id = uuid();
									Filesystem.writeFile({
										path: `smart-ads-files/${id}.${image.format}`,
										data: image.dataUrl,
										directory: FilesystemDirectory.External,
										encoding: FilesystemEncoding.UTF8
									}).then((result) => {
										this.updateStorageImage(
											image.dataUrl,
											`smart-ads-files/${id}.${image.format}`,
											this.form.value
										);
									});
								})
								.catch((err) => {
									this.onError('Napaka', 'Prišlo je do napake pri pridobivanju slike.');
								});
						}
					}
				]
			})
			.then((sheetEl) => {
				sheetEl.present();
			});
	}

	updateStoragePdf(filePath, formValues) {
		this.notes[formValues.noteGroup].courseNotes.push({
			path: filePath,
			src: 'assets/img/pdf.png',
			title: formValues.title,
			date: new Date(),
			description: formValues.description,
			deleteQueue: false,
			type: 'pdf'
		});

		Storage.set({ key: 'smart-ads-zapiski', value: JSON.stringify(this.notes) }).then((_) => {
			this.modalCtrl.dismiss({ notes: this.notes });
		});
	}

	updateStorageImage(src, filePath, formValues) {
		this.notes[formValues.noteGroup].courseNotes.push({
			path: filePath,
			src: null,
			title: formValues.title,
			date: new Date(),
			description: formValues.description,
			deleteQueue: false,
			type: 'pic'
		});

		Storage.set({ key: 'smart-ads-zapiski', value: JSON.stringify(this.notes) }).then((_) => {
			const length = this.notes[formValues.noteGroup].courseNotes.length;
			this.notes[formValues.noteGroup].courseNotes[length - 1].src = src;
			this.modalCtrl.dismiss({ notes: this.notes });
		});
	}

	onError(header: string, message: string) {
		this.alertCtrl
			.create({
				header,
				message,
				buttons: [ 'Zapri' ]
			})
			.then((alert) => {
				alert.present();
			});
	}

	closeModal() {
		this.modalCtrl.dismiss(null);
	}
}
