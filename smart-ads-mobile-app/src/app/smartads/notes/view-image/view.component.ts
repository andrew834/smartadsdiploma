import { Component, Input, OnDestroy } from '@angular/core';
import { PinchZoomComponent } from 'ngx-pinch-zoom/lib/pinch-zoom.component';

@Component({
	templateUrl: './view.component.html',
	styleUrls: [ './view.component.scss' ]
})
export class ViewNoteComponent implements OnDestroy {
	@Input() image;

	constructor() {}

	ionViewDidEnter(){
		setTimeout(() => {
			screen.orientation.unlock();
		}, 750);
	}

	ngOnDestroy() {
		screen.orientation.lock('portrait');
	}
}
