import { Component, OnInit, OnDestroy } from '@angular/core';
import { ContextService } from 'src/app/shared/services/context.service';
import { ToastController } from '@ionic/angular';
import { Subscription } from 'rxjs';

@Component({
	templateUrl: './profile.page.html',
	styleUrls: [ './profile.page.scss' ]
})
export class ProfilePage implements OnInit, OnDestroy {
	contextSubscription: Subscription;
	constructor(private contextService: ContextService, private toastCtrl: ToastController){}

	ngOnInit() {
		setTimeout((_) => {
			this.contextSubscription = this.contextService.context.subscribe(context => {
				this.toastCtrl.create({
					message: 
						`Activity name: ${context.activity.name}, confidence: ${context.activity.confidence}\n` +
						`Connection connected: ${context.connection.connected}, connectionType: ${context.connection.connectionType}\n` +
						`Battery status: ${context.battery.status}, level: ${context.battery.level}, isPlugged: ${context.battery.isPlugged}\n` +
						`Brightness status: ${context.brightness.status}, v: ${context.brightness.value}\n` +
						`Time status: ${context.time.status}, date: ${context.time.date.toISOString()}\n` + 
						`Current location lat: ${context.userLocation.currentLocation.lat}, lng: ${context.userLocation.currentLocation.lng} \n` +
						`Location status: ${context.userLocation.currentLocation.status}`,
						duration: 20000
				}).then(toastEl => {
					toastEl.present();
				});
			});
		}, 200);
	}

	ngOnDestroy() {
		if (this.contextSubscription) {
			this.contextSubscription.unsubscribe();
			this.toastCtrl.dismiss();
		}
	}
}
