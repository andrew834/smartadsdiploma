import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { ToolsPage } from './tools.page';

const routes: Routes = [
	{
		path: '',
		component: ToolsPage
	}
];

@NgModule({
	imports: [ CommonModule, IonicModule, RouterModule.forChild(routes) ],
	declarations: [ ToolsPage ]
})
export class ToolsPageModule {
	constructor() {}
}
