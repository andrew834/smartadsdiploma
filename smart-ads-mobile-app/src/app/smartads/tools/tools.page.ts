import { Component, OnInit } from '@angular/core';
import { Plugins } from '@capacitor/core';
import { AlertController } from '@ionic/angular';
import { ActionsService } from 'src/app/shared/services/actions.service';

const { Browser, Storage } = Plugins;

@Component({
	templateUrl: './tools.page.html',
	styleUrls: [ './tools.page.scss' ]
})
export class ToolsPage implements OnInit {
	tools = [
		{
			category: 'Splošno',
			name: 'Stackoverflow',
			url: 'https://stackoverflow.com/'
		},
		{
			category: 'Splošno',
			name: 'Wolframalpha',
			url: 'https://www.wolframalpha.com/'
		},
		{
			category: 'Odvajanje',
			name: 'Derivative Calculator',
			url: 'https://www.derivative-calculator.net/'
		},
		{
			category: 'Integriranje',
			name: 'Integral Calculator',
			url: 'https://www.integral-calculator.com/'
		},
		{
			category: 'Risanje grafov',
			name: 'Geogebra',
			url: 'https://www.geogebra.org/graphing'
		},
		{
			category: 'Risanje',
			name: 'AutoDraw',
			url: 'https://www.autodraw.com/'
		},
		{
			category: 'Računanje',
			name: 'Desmos Calculator',
			url: 'https://www.desmos.com/scientific'
		},
		{
			category: 'Logika',
			name: 'Boolean Algebra',
			url: 'http://electronics-course.com/boolean-algebra'
		},
		{
			category: 'Splošno',
			name: 'Wikipedia',
			url: 'https://www.wikipedia.org/'
		}
	];

	constructor(private alertCtrl: AlertController, private actionsService: ActionsService) {}

	ngOnInit() {
		Storage.get({ key: 'smartads-tools' }).then((data) => {
			let storageTools = JSON.parse(data.value);
			if (storageTools.length !== 0) {
				this.tools = storageTools;
			}
		});
	}

	onOpenTool(url) {
		Browser.open({ url });
	}

	newTool() {
		this.alertCtrl
			.create({
				header: 'Dodaj pripomoček',
				message: 'Shrani nov pripomoček, ki ga boš pogosto uporabljal/a.',
				inputs: [
					{
						name: 'category',
						placeholder: 'Kategorija'
					},
					{
						name: 'name',
						placeholder: 'Naslov'
					},
					{
						name: 'url',
						placeholder: 'Link (oblike www.google.com)'
					}
				],
				buttons: [
					{
						text: 'Prekliči',
						role: 'cancel',
						handler: (_) => {
							this.alertCtrl.dismiss();
						}
					},
					{
						text: 'Shrani',
						handler: (data) => {
							if (data.url.length !== 0) {
								if (!data.url.includes('http')) {
									data.url = `https://${data.url}`;
								}
							}

							this.tools.push(data);
							Storage.set({
								key: 'smartads-tools',
								value: JSON.stringify(this.tools)
							})
								.then((_) => {
									this.alertCtrl.dismiss();
								})
								.catch((err) => {
									console.log(err);
									this.alertCtrl.dismiss();
								});

								
							this.actionsService.actionFiredShowAd();
						}
					}
				]
			})
			.then((alertEl) => {
				alertEl.present();
			});
	}

	onDelete(tool, index) {
		this.alertCtrl
			.create({
				header: 'Brisanje pripomočka',
				message: 'Ali ste prepričani, da želite izbrisati ta pripomoček?',
				buttons: [
					{
						text: 'Prekini',
						role: 'cancel'
					},
					{
						text: 'Izbriši',
						handler: (_) => {
							this.alertCtrl.dismiss();
							this.tools.splice(index, 1);
							Storage.set({
								key: 'smartads-tools',
								value: JSON.stringify(this.tools)
							}).catch((err) => {
								console.log(err);
							});

							this.actionsService.actionFiredShowAd();
						}
					}
				]
			})
			.then((alertEl) => {
				alertEl.present();
			});
	}
}
