import { Component, OnInit } from '@angular/core';
import * as xmlToJson from 'xml2js';
import { HttpService } from 'src/app/shared/services/http.service';
import { PopoverController, NavController } from '@ionic/angular';
import { PopoverNewsComponent } from './popover/popover.component';
import { ActionsService } from 'src/app/shared/services/actions.service';
import { ThrowStmt } from '@angular/compiler';

@Component({
	templateUrl: './news.page.html',
	styleUrls: [ './news.page.scss' ]
})
export class NewsPage implements OnInit {
	private RSS_FEEDS = [
		{
			url: 'https://www.student.si/feed/',
			name: 'Student.si'
		},
		{
			url: 'https://www.mojprihranek.si/feed/',
			name: 'Moj Prihranek'
		},
		{
			url: 'https://svet.fri.uni-lj.si/feed/',
			name: 'Študentski svet FRI'
		}
	];

	currentRssFeed;
	news = [];

	constructor(private httpService: HttpService, private popoverController: PopoverController, private actionsService: ActionsService) {}

	ngOnInit() {
		this.getNews(null, this.RSS_FEEDS[0].url);
	}

	getNews(event, url) {
		if (event == null) {
			this.news = [];
		}

		this.currentRssFeed = url;
		this.httpService.getNewsFeed(url).subscribe(
			(xmlData) => {
				try {
					xmlToJson.parseString(xmlData, (err, result) => {
						if (err) {
							throw err;
						}

						this.news = result.rss.channel[0].item;

						if (event != null) {
							setTimeout(() => {
								event.target.complete();
							}, 500);
						}
					});
				} catch (e) {
					console.log(e);
				}
			},
			(serverErr) => {
				console.log(serverErr);
			}
		);
	}

	changeRssFeed(event) {
		this.popoverController
			.create({
				component: PopoverNewsComponent,
				translucent: false,
				event,
				componentProps: {
					feeds: this.RSS_FEEDS
				}
			})
			.then((popoverEl) => {
				popoverEl.present();
				return popoverEl.onDidDismiss().then((returnData) => {
					if (returnData.data != null) {
						this.currentRssFeed = returnData.data.url;
						this.getNews(null, returnData.data.url);
						this.actionsService.actionFiredShowAd();
					}
				});
			});
	}
}
