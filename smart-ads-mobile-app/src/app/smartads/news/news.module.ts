import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { NewsPage } from './news.page';
import { PopoverNewsComponent } from './popover/popover.component';

const routes: Routes = [
	{
		path: '',
		component: NewsPage
	}
];

@NgModule({
	imports: [ CommonModule, IonicModule, RouterModule.forChild(routes) ],
	declarations: [ NewsPage, PopoverNewsComponent ],
	entryComponents: [ PopoverNewsComponent ]
})
export class NewsPageModule {
	constructor() {}
}
