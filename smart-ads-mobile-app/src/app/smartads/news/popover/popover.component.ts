import { Component } from '@angular/core';
import { PopoverController, NavParams } from '@ionic/angular';

@Component({
	templateUrl: './popover.component.html',
	styleUrls: [ './popover.component.scss' ]
})
export class PopoverNewsComponent {
	feeds = [];

	constructor(private navParams: NavParams, private popoverCtrl: PopoverController) {
		this.feeds = navParams.get('feeds');
	}

	onFeedSelected(feedUrl) {
		this.popoverCtrl.dismiss({ url: feedUrl });
	}
}
