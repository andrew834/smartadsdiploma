import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import {
	MatInputModule,
	MatCardModule,
	MatButtonModule,
	MatToolbarModule,
	MatExpansionModule,
	MatProgressSpinnerModule,
	MatPaginatorModule,
	MatDialogModule,
	MatOptionModule,
	MatSelectModule,
	MatDividerModule,
	MatCheckboxModule,
	MatSlideToggleModule,
	MatTableModule
} from '@angular/material';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CdkTableModule } from '@angular/cdk/table';
import { TrainPage } from './train.page';

const routes: Routes = [
	{
		path: '',
		component: TrainPage
	}
];

@NgModule({
	imports: [
		CommonModule,
		IonicModule,
		RouterModule.forChild(routes),
		MatInputModule,
		MatCardModule,
		MatButtonModule,
		MatFormFieldModule,
		MatToolbarModule,
		MatTableModule,
		MatExpansionModule,
		CdkTableModule,
		MatProgressSpinnerModule,
		MatPaginatorModule,
		MatDialogModule,
		MatOptionModule,
		MatSelectModule,
		MatDividerModule,
		MatCheckboxModule,
		MatSlideToggleModule,
		MatAutocompleteModule,
		FormsModule,
		ReactiveFormsModule
	],
	declarations: [ TrainPage ]
})
export class TrainPageModule {
	constructor() {}
}
