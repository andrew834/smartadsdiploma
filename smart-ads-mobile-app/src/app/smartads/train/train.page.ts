import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { HttpService } from 'src/app/shared/services/http.service';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { DataSource } from '@angular/cdk/table';
import { Plugins } from '@capacitor/core';
import { ActionsService } from 'src/app/shared/services/actions.service';

const { Browser } = Plugins;

@Component({
	templateUrl: './train.page.html',
	styleUrls: [ './train.page.scss' ]
})
export class TrainPage implements OnInit, AfterViewInit {
	@ViewChild('trainStationIn', { static: false })
	@ViewChild('trainStationOut', { static: false })
	trainStationIn;
	trainStationOut;

	stationIdIn = '';
	stationIdOut = '';
	requestedDate = new Date();

	trainSearchInControl = new FormControl();
	trainSearchOutControl = new FormControl();

	filteredOptionsIn: Observable<string[]>;
	filteredOptionsOut: Observable<string[]>;

	stationsInHm = {};
	stationsOutHm = {};
	stationsInArr = [];
	stationsOutArr = [];

	isLoaded = true;

	constructor(private httpService: HttpService, private actionsService: ActionsService) {}

	ngOnInit() {
		this.httpService.getTrainStations('in').subscribe((serverRes) => {
			this.stationsInHm = serverRes.t;
			this.stationsInArr = this.hashmapToArray(serverRes.t);
		});

		this.httpService.getTrainStations('out').subscribe((serverRes) => {
			this.stationsOutHm = serverRes.t;
			this.stationsOutArr = this.hashmapToArray(serverRes.t);
		});

		this.filteredOptionsIn = this.trainSearchInControl.valueChanges.pipe(
			startWith(''),
			map((value) => this._filterIn(value))
		);

		this.filteredOptionsOut = this.trainSearchOutControl.valueChanges.pipe(
			startWith(''),
			map((value) => this._filterOut(value))
		);
	}

	private _filterIn(value: string): string[] {
		const filterValue = value.toLowerCase();
		return this.stationsInArr.filter((station) => station.toLowerCase().indexOf(filterValue) === 0);
	}

	private _filterOut(value: string): string[] {
		const filterValue = value.toLowerCase();
		return this.stationsOutArr.filter((station) => station.toLowerCase().indexOf(filterValue) === 0);
	}

	ngAfterViewInit() {
		this.isLoaded = true;
		this.actionsService.actionFiredShowAd();
	}

	hashmapToArray(hashmap) {
		const array = [];
		let i = 0;

		for (const key in hashmap) {
			array[i] = `${hashmap[key]} (${key})`;
			i++;
		}

		return array;
	}

	changeSelectedOption(option, direction: string) {
		try {
			const stationId = option.substring(option.lastIndexOf('(') + 1, option.lastIndexOf(')'));

			if (direction === 'in') {
				this.stationIdIn = stationId;
			} else {
				this.stationIdOut = stationId;
			}
		} catch (err) {
			console.log(err);
		}
	}

	getTrainData() {
		const rawDate = this.requestedDate.toISOString();
		const date = `${rawDate.split('T')[0]}T00%3A00%3A00`;

		const url = `https://www.slo-zeleznice.si/sl/component/sz_timetable/?vs=${this.stationIdIn}&vi=&iz=${this
			.stationIdOut}&da=${date}`;

		Browser.open({ url });
	}
}
