import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { PopoverController, ModalController, AlertController } from '@ionic/angular';
import { PopoverTimeslotComponent } from './popover/popover.component';
import { CreateComponent } from './create/create.component';
import { HttpService } from 'src/app/shared/services/http.service';
import * as uuid from 'uuid';
import { UpdateComponent } from './update/update.component';
import { ActionsService } from 'src/app/shared/services/actions.service';

@Component({
	templateUrl: 'timetable.page.html',
	styleUrls: [ 'timetable.page.scss' ]
})
export class TimetablePage implements OnInit {
	hours = [
		'07:00',
		'08:00',
		'09:00',
		'10:00',
		'11:00',
		'12:00',
		'13:00',
		'14:00',
		'15:00',
		'16:00',
		'17:00',
		'18:00',
		'19:00',
		'20:00',
		'21:00'
	];

	eventsPon = [];
	eventsTor = [];
	eventsSre = [];
	eventsCet = [];
	eventsPet = [];

	modifyServerData(events) {
		let eventProjection = [];
		let currentDuration = 0;
		for (let i = 0; i < events.table.length; i++) {
			let tempEvent = Object.create(null);
			if (currentDuration === 0) {
				if (events.table[i].duration !== 0) {
					tempEvent.t = events.table[i].time;
					tempEvent.n = events.table[i].course;
					tempEvent.d = events.table[i].duration;
					tempEvent.p = events.table[i].classroom;
					tempEvent.c = this.getRandomRGBA();
					tempEvent._id = uuid.v4();
					currentDuration = events.table[i].duration - 1;
				} else {
					tempEvent.t = events.table[i].time;
					tempEvent.n = '';
					tempEvent.d = 0;
					tempEvent.p = '';
					tempEvent.c = '';
					tempEvent._id = '';
				}
			} else {
				tempEvent.t = events.table[i].time;
				tempEvent.n = events.table[i].course;
				tempEvent.d = events.table[i].duration;
				tempEvent.p = events.table[i].classroom;
				tempEvent.c = eventProjection[i - 1].c;
				tempEvent._id = eventProjection[i - 1]._id;
				currentDuration--;
			}
			eventProjection.push(tempEvent);
		}
		return eventProjection;
	}

	ngOnInit() {
		this.requestMyData();
	}

	requestMyData() {
		this.httpService.getMyTimetable().subscribe(
			(serverRes) => {
				this.eventsPon = this.modifyServerData(serverRes.t[0]);
				this.eventsTor = this.modifyServerData(serverRes.t[1]);
				this.eventsSre = this.modifyServerData(serverRes.t[2]);
				this.eventsCet = this.modifyServerData(serverRes.t[3]);
				this.eventsPet = this.modifyServerData(serverRes.t[4]);

				this.changeDetector.detectChanges();
			},
			(serverErr) => {
				console.log(serverErr);
				this.onError(
					'Oh ne!',
					serverErr.error && serverErr.error.m
						? serverErr.error.m
						: 'Nekaj je šlo narobe. Poiskusite kasneje.'
				);
			}
		);
	}

	constructor(
		private popoverController: PopoverController,
		private modalCtrl: ModalController,
		private httpService: HttpService,
		private alertCtrl: AlertController,
		private changeDetector: ChangeDetectorRef,
		private actionsService: ActionsService
	) {}

	checkPreviousEvent(events, index) {
		if (index === 0) {
			if (events.length === 0) {
				return '';
			}
			return events[index].n;
		}

		if (index >= events.length) {
			return '';
		}

		if (events[index - 1].n === events[index].n) {
			return '';
		} else {
			return events[index].n;
		}
	}

	checkPreviousEventForColor(events, index) {
		if (index === 0) {
			if (events.length === 0) {
				return '';
			} else {
				if (events[index].n !== '') {
					return events[index].c;
				} else {
					return '';
				}
			}
		} else {
			if (index >= events.length) {
				return '';
			} else {
				if (events[index].n !== '') {
					if (events[index].n === events[index - 1].n) {
						return events[index - 1].c;
					} else {
						return events[index].c;
					}
				} else {
					return '';
				}
			}
		}
	}

	getRandomRGBA() {
		const r = Math.floor(Math.random() * 1) + 10;
		const g = Math.floor(Math.random() * 106) + 100;
		const b = Math.floor(Math.random() * 106) + 150;
		const a = 0.7;
		return `rgba(${r}, ${g}, ${b}, ${a})`;
	}

	checkForBorder(events, index) {
		if (index < events.length) {
			if (events[index].n !== '') {
				return 'no-border';
			}
		}

		return 'border';
	}

	onEditEvent(events, index, domEvent, day) {
		if (index < events.length) {
			if (events[index].n !== '') {
				this.editSlot(this.findStartSlotIndex(events, index), domEvent, day, events[index].n);
			}
		}
	}

	findStartSlotIndex(events, index) {
		const slotId = events[index]._id;
		for (let i = 0; i < events.length; i++) {
			if (events[i]._id === slotId) {
				return i;
			}
		}
	}

	editSlot(timeslotIndex: number, event, day: string, course: string) {
		this.popoverController
			.create({
				component: PopoverTimeslotComponent,
				translucent: false,
				event,
				componentProps: {
					index: timeslotIndex,
					day,
					course
				}
			})
			.then((popoverEl) => {
				popoverEl.present();
				let eventsArr = [];
				switch (day) {
					case 'Pon':
						eventsArr = this.eventsPon;
						break;
					case 'Tor':
						eventsArr = this.eventsTor;
						break;
					case 'Sre':
						eventsArr = this.eventsSre;
						break;
					case 'Cet':
						eventsArr = this.eventsCet;
						break;
					case 'Pet':
						eventsArr = this.eventsPet;
						break;
				}

				return popoverEl.onDidDismiss().then((returnData) => {
					if (returnData.data != null) {
						switch (returnData.data.e) {
							case 'delete':
								this.alertCtrl
								.create({
									header: 'Brisanje predmeta',
									message: 'Ali ste prepričani, da želite izbrisati predmet?',
									buttons: [
										{
											text: 'Prekliči',
											role: 'cancel'
										},
										{
											text: 'Izbriši',
											handler: (_) => {
												this.alertCtrl.dismiss();
												this.onRequestDelete(returnData.data.day, returnData.data.index, eventsArr);
												this.actionsService.actionFiredShowAd();
											}
										}
									]
								}).then(alertEl => {
									alertEl.present();
								});
								break;
							case 'edit':
								this.onRequestEdit(returnData.data.day, returnData.data.index);
								break;
						}
					}
				});
			});
	}

	onRequestEdit(day, index) {
		this.modalCtrl
			.create({
				component: UpdateComponent,
				componentProps: {
					events: {
						Pon: this.eventsPon,
						Tor: this.eventsTor,
						Sre: this.eventsSre,
						Cet: this.eventsCet,
						Pet: this.eventsPet,
						day: day,
						index: index
					}
				}
			})
			.then((modalEl) => {
				modalEl.present();

				return modalEl.onDidDismiss().then((modalData) => {
					if (modalData.data != null) {
						if (modalData.data.e === 'fresh') {
							this.requestMyData();
							this.actionsService.actionFiredShowAd();
						}
					}
				});
			});
	}

	onRequestDelete(day, index, eventsArr) {
		this.httpService.removeCourse(index, day).subscribe(
			() => {
				let endIndex = index + eventsArr[index].d;

				for (let i = index; i < endIndex; i++) {
					eventsArr[i].c = '';
					eventsArr[i].d = 0;
					eventsArr[i].n = '';
					eventsArr[i].p = '';
				}

				this.changeDetector.detectChanges();
			},
			(serverErr) => {
				console.log(serverErr);
				this.onError(
					'Oh ne!',
					serverErr.error && serverErr.error.m
						? serverErr.error.m
						: 'Nekaj je šlo narobe. Poiskusite kasneje.'
				);
			}
		);
	}

	newTimeSlot() {
		this.modalCtrl
			.create({
				component: CreateComponent,
				componentProps: {
					events: {
						Pon: this.eventsPon,
						Tor: this.eventsTor,
						Sre: this.eventsSre,
						Cet: this.eventsCet,
						Pet: this.eventsPet
					}
				}
			})
			.then((modalEl) => {
				modalEl.present();

				return modalEl.onDidDismiss().then((modalData) => {
					if (modalData.data != null) {
						if (modalData.data.e === 'fresh') {
							this.requestMyData();
							this.actionsService.actionFiredShowAd();
						}
					}
				});
			});
	}

	onError(header: string, message: string) {
		this.alertCtrl
			.create({
				header,
				message,
				buttons: [ 'Zapri' ]
			})
			.then((alert) => {
				alert.present();
			});
	}
}
