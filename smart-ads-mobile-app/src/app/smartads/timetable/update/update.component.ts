import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';

import { AlertController, ModalController, LoadingController } from '@ionic/angular';
import { HttpService } from 'src/app/shared/services/http.service';

@Component({
	templateUrl: './update.component.html',
	styleUrls: [ './update.component.scss' ]
})
export class UpdateComponent implements OnInit {
	@Input() events;
	form: FormGroup;
	prevDayIndex = 0;

	constructor(
		private alertCtrl: AlertController,
		private modalCtrl: ModalController,
		private httpService: HttpService,
		private loadingCtrl: LoadingController
	) {}

	ngOnInit() {
		const eventData = this.events[this.events.day][this.events.index];
		switch (this.events.day) {
			case 'Pon':
				this.prevDayIndex = 0;
				break;
			case 'Tor':
				this.prevDayIndex = 1;
				break;
			case 'Sre':
				this.prevDayIndex = 2;
				break;
			case 'Cet':
				this.prevDayIndex = 3;
				break;
			case 'Pet':
				this.prevDayIndex = 4;
				break;
		}

		this.form = new FormGroup({
			course: new FormControl(eventData.n, {
				updateOn: 'change',
				validators: [ Validators.required, Validators.minLength(1), Validators.maxLength(7) ]
			}),
			classroom: new FormControl(eventData.p, {
				updateOn: 'change',
				validators: [ Validators.required, Validators.minLength(1), Validators.maxLength(7) ]
			}),
			duration: new FormControl(eventData.d, {
				updateOn: 'change',
				validators: [ Validators.required, Validators.min(1), Validators.max(5) ]
			}),
			time: new FormControl(eventData.t, {
				updateOn: 'change',
				validators: [ Validators.required ]
			}),
			day: new FormControl(this.events.day, {
				updateOn: 'change',
				validators: [ Validators.required ]
			})
		});
	}

	onUpdateTimeSlot() {
		if (!this.form.valid) {
			this.onError('Oh ne!', 'Nekatera polja so neustrezno izpolnjena.');
			return;
		}

		const timeInt = parseInt(this.form.value.time.split(':')[0]);
		let timeslotData = this.form.value;
		timeslotData._id = this.events[this.events.day][this.events.index]._id;
		timeslotData.prevDayIndex = this.prevDayIndex;

		if (timeInt < 10) {
			timeslotData.time = `0${timeInt}:00`;
		} else {
			timeslotData.time = `${timeInt}:00`;
		}

		if (!this.isTimeValid(timeslotData)) {
			this.onError('Oh ne!', 'Napaka pri shranjevanju. Predmeti lahko trajajo le do 22:00.');
			return;
		}

		if (this.isOverlaping(timeslotData)) {
			this.onError('Oh ne!', 'Napaka pri shranjevanju. Prišlo je do prekrivanj.');
			return;
		}

		this.loadingCtrl
			.create({
				keyboardClose: true,
				duration: 6000,
				message: 'Posodabljanje predmeta...'
			})
			.then((loader) => {
				timeslotData.index = this.events.index;
				loader.present();

				this.httpService.updateCourse(timeslotData).subscribe(
					(serverRes) => {
						loader.dismiss();
						this.modalCtrl.dismiss({ e: 'fresh' });
					},
					(serverErr) => {
						loader.dismiss();
						this.onError(
							'Oh ne!',
							serverErr.error && serverErr.error.m
								? serverErr.error.m
								: 'Nekaj je šlo narobe. Poiskusite kasneje.'
						);
					}
				);
			});
	}

	getRandomRGBA() {
		const r = Math.floor(Math.random() * 1) + 10;
		const g = Math.floor(Math.random() * 106) + 100;
		const b = Math.floor(Math.random() * 106) + 150;
		const a = 0.7;
		return `rgba(${r}, ${g}, ${b}, ${a})`;
	}

	isTimeValid(data) {
		const time = parseInt(data.time.split(':')[0]);
		if (data.duration + time <= 22) {
			return true;
		} else {
			return false;
		}
	}

	isOverlaping(data) {
		let startTime = parseInt(data.time.split(':')[0]);
		let eventsDayArr = this.events[data.day];
		let requestedDuration = data.duration;

		let endTime = startTime - 7 + requestedDuration;
		for (let i = startTime - 7; i < endTime; i++) {
			if (eventsDayArr[i]._id === data._id) {
				continue;
			}

			if (eventsDayArr[i].d !== 0) {
				return true;
			}
		}

		return false;
	}

	onError(header: string, message: string) {
		this.alertCtrl
			.create({
				header,
				message,
				buttons: [ 'Zapri' ]
			})
			.then((alert) => {
				alert.present();
			});
	}

	closeModal() {
		this.modalCtrl.dismiss(null);
	}
}
