import { Component } from '@angular/core';
import { PopoverController, NavParams } from '@ionic/angular';

@Component({
	templateUrl: './popover.component.html',
	styleUrls: [ './popover.component.scss' ]
})
export class PopoverTimeslotComponent {
	index = null;
	day = null;
	course = '';

	constructor(private navParams: NavParams, private popoverCtrl: PopoverController) {
		this.day = navParams.get('day');
		this.index = navParams.get('index');
		this.course = navParams.get('course');
	}

	onEdit() {
		this.popoverCtrl.dismiss({ e: 'edit', day: this.day, index: this.index });
	}

	onDelete() {
		this.popoverCtrl.dismiss({ e: 'delete', day: this.day, index: this.index });
	}
}
