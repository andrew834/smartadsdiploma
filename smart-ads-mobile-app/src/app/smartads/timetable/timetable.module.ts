import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TimetablePage } from './timetable.page';
import { PopoverTimeslotComponent } from './popover/popover.component';
import { CreateComponent } from './create/create.component';
import { UpdateComponent } from './update/update.component';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		IonicModule,
		RouterModule.forChild([
			{
				path: '',
				component: TimetablePage
			}
		])
	],
	declarations: [ TimetablePage, PopoverTimeslotComponent, CreateComponent, UpdateComponent ],
	entryComponents: [ PopoverTimeslotComponent, CreateComponent, UpdateComponent ]
})
export class TimetablePageModule {}
