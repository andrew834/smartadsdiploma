import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';

import { AlertController, ModalController, LoadingController } from '@ionic/angular';
import { HttpService } from 'src/app/shared/services/http.service';

@Component({
	templateUrl: './create.component.html',
	styleUrls: [ './create.component.scss' ]
})
export class CreateComponent implements OnInit {
	@Input() events;
	form: FormGroup;

	constructor(
		private alertCtrl: AlertController,
		private modalCtrl: ModalController,
		private httpService: HttpService,
		private loadingCtrl: LoadingController
	) {}

	ngOnInit() {
		this.form = new FormGroup({
			course: new FormControl(null, {
				updateOn: 'change',
				validators: [ Validators.required, Validators.minLength(1), Validators.maxLength(7) ]
			}),
			classroom: new FormControl(null, {
				updateOn: 'change',
				validators: [ Validators.required, Validators.minLength(1), Validators.maxLength(7) ]
			}),
			duration: new FormControl(3, {
				updateOn: 'change',
				validators: [ Validators.required, Validators.min(1), Validators.max(5) ]
			}),
			time: new FormControl('12:00', {
				updateOn: 'change',
				validators: [ Validators.required ]
			}),
			day: new FormControl('Pon', {
				updateOn: 'change',
				validators: [ Validators.required ]
			})
		});
	}

	onCreateTimeSlot() {
		if (!this.form.valid) {
			this.onError('Oh ne!', 'Nekatera polja so neustrezno izpolnjena.');
			return;
		}

		const timeInt = parseInt(this.form.value.time.split(':')[0]);
		let timeslotData = this.form.value;

		if (timeInt < 10) {
			timeslotData.time = `0${timeInt}:00`;
		} else {
			timeslotData.time = `${timeInt}:00`;
		}

		if (!this.isTimeValid(timeslotData)) {
			this.onError('Oh ne!', 'Napaka pri shranjevanju. Predmeti lahko trajajo le do 22:00.');
			return;
		}

		if (this.isOverlaping(timeslotData)) {
			this.onError('Oh ne!', 'Napaka pri shranjevanju. Prišlo je do prekrivanj.');
			return;
		}

		this.loadingCtrl
			.create({
				keyboardClose: true,
				duration: 6000,
				message: 'Kreiranje predmeta...'
			})
			.then((loader) => {
				let tempData = timeslotData;
				tempData._id = '';
				loader.present();

				this.httpService.addNewCourse(timeslotData).subscribe(
					(serverRes) => {
						loader.dismiss();
						this.modalCtrl.dismiss({ e: 'fresh' });
					},
					(serverErr) => {
						loader.dismiss();
						this.onError(
							'Oh ne!',
							serverErr.error && serverErr.error.m
								? serverErr.error.m
								: 'Nekaj je šlo narobe. Poiskusite kasneje.'
						);
					}
				);
			});
	}

	getRandomRGBA() {
		const r = Math.floor(Math.random() * 1) + 10;
		const g = Math.floor(Math.random() * 106) + 100;
		const b = Math.floor(Math.random() * 106) + 150;
		const a = 0.7;
		return `rgba(${r}, ${g}, ${b}, ${a})`;
	}

	isTimeValid(data) {
		const time = parseInt(data.time.split(':')[0]);
		if (data.duration + time <= 22) {
			return true;
		} else {
			return false;
		}
	}

	isOverlaping(data) {
		let startTime = parseInt(data.time.split(':')[0]);
		let eventsDayArr = this.events[data.day];
		let requestedDuration = data.duration;

		let endTime = startTime - 7 + requestedDuration;
		for (let i = startTime - 7; i < endTime; i++) {
			if(eventsDayArr[i] == null){
				this.modalCtrl.dismiss(null);
				setTimeout(() => {
					this.onError("Oh ne!", "Prišlo je do neznane napake, prosimo poskusite kasneje.");
				}, 500);
				throw "Prišlo je do napake";
			}

			if (eventsDayArr[i].d !== 0) {
				return true;
			}
		}

		return false;
	}

	onError(header: string, message: string) {
		this.alertCtrl
			.create({
				header,
				message,
				buttons: [ 'Zapri' ]
			})
			.then((alert) => {
				alert.present();
			});
	}

	closeModal() {
		this.modalCtrl.dismiss(null);
	}
}
