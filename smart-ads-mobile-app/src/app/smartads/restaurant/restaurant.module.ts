import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { RestaurantPage } from './restaurant.page';

const routes: Routes = [
	{
		path: '',
		component: RestaurantPage
	}
];

@NgModule({
	imports: [ CommonModule, IonicModule, RouterModule.forChild(routes) ],
	declarations: [ RestaurantPage ]
})
export class RestaurantPageModule {
	constructor() {}
}
