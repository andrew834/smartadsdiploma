import { Component, AfterViewInit, ViewChild, ElementRef, OnInit } from '@angular/core';
import { HttpService } from 'src/app/shared/services/http.service';
import { AlertController, NavController } from '@ionic/angular';
import { ActionsService } from 'src/app/shared/services/actions.service';

@Component({
	templateUrl: './restaurant.page.html',
	styleUrls: [ './restaurant.page.scss' ]
})
export class RestaurantPage implements OnInit, AfterViewInit {
	@ViewChild('googleMap', { static: false })
	googleMapRef: ElementRef;

	UPDATE_PERIOD_TIME = 10000;

	centerdMapOnlyOnce = false;

	googleMap: google.maps.Map;
	defaultCoordinates = {
		lat: 46.056946,
		lng: 14.505751
	};

	canLoadRestaurants = false;
	currentBoni = [];

	currentUserCoordinates = new google.maps.LatLng(this.defaultCoordinates.lat, this.defaultCoordinates.lng);
	currentUserMarker = new google.maps.Marker({
		position: this.currentUserCoordinates,
		map: this.googleMap,
		icon: 'assets/icon/me-custom.png',
		title: 'Nahajate se tukaj'
	});

	constructor(private httpService: HttpService, private alertCtrl: AlertController, private actionsService: ActionsService) {}

	mapOptions: google.maps.MapOptions = {
		center: this.currentUserCoordinates,
		zoom: 16
	};

	ngOnInit() {
		this.httpService.getAllBoni().subscribe((boni) => {
			if (boni && boni.b) {
				this.currentBoni = boni.b;
				if (this.canLoadRestaurants) {
					this.showRestaurants();
				}
			} else {
				this.alertCtrl
					.create({
						header: 'Oh ne!',
						message: 'Strežnik nima shranjenih restavracij za bone.',
						buttons: [ 'Zapri' ]
					})
					.then((alert) => {
						alert.present();
					});
			}
		});
	}

	ngAfterViewInit() {
		this.initializeMap();
		this.actionsService.actionFiredShowAd();
	}

	initializeMap() {
		this.googleMap = new google.maps.Map(this.googleMapRef.nativeElement, this.mapOptions);
		this.canLoadRestaurants = true;
		this.getUserLocation();
	}

	getUserLocation() {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(this.onPosition, this.onError);
		} else {
			this.alertCtrl
				.create({
					header: 'Oh ne!',
					message: 'Naprava ne podpira geolokacije.',
					buttons: [ 'Zapri' ]
				})
				.then((alert) => {
					this.currentUserMarker.setMap(null);
					this.currentUserMarker.setMap(this.googleMap);
					alert.present();
				});
		}
	}

	onPosition = (userPosition) => {
		this.currentUserCoordinates = new google.maps.LatLng(
			userPosition.coords.latitude,
			userPosition.coords.longitude
		);
		this.currentUserMarker.setMap(null);
		this.currentUserMarker = new google.maps.Marker({
			position: this.currentUserCoordinates,
			map: this.googleMap,
			icon: 'assets/icon/me-custom.png',
			title: 'Nahajate se tukaj'
		});

		if (!this.centerdMapOnlyOnce) {
			this.onCenterMap(this.currentUserCoordinates.lat(), this.currentUserCoordinates.lng());
			this.centerdMapOnlyOnce = true;
		}
	};

	onError = (_) => {
		this.onCenterMap(this.defaultCoordinates.lat, this.defaultCoordinates.lng);
		this.currentUserMarker.setMap(null);
	};

	onCenterMap(lat, lng) {
		if (this.googleMap) {
			this.googleMap.setCenter(new google.maps.LatLng(lat, lng));

			if (this.canLoadRestaurants && this.currentBoni.length !== 0) {
				this.showRestaurants();
			}
		}
	}

	showRestaurants() {
		this.canLoadRestaurants = false;
		if (this.currentBoni.length !== 0) {
			
			for (let i = 0; i < this.currentBoni.length; i++) {
				this.setMarker(this.currentBoni[i]);
			}
		}
	}

	setMarker(restaurant) {
		const calculateDistance = this.getDistance(
			this.currentUserCoordinates.lat(),
			this.currentUserCoordinates.lng(),
			restaurant.lat,
			restaurant.lng
		);

		if (calculateDistance < 200) {
			let stars = '';
			let starRating = '';

			if (restaurant.rating) {
				starRating = restaurant.rating;
			}

			for (let i = 0; i < Math.floor(restaurant.rating); i++) {
				stars += `<ion-icon name="star" style="font-size: 14px;" color="warning"></ion-icon>`;
			}

			if (Math.floor(restaurant.rating) !== Math.ceil(restaurant.rating)) {
				stars += `<ion-icon name="star-half" style="font-size: 14px;" color="warning"></ion-icon>`;
			}

			const contentString = `<div>
                    <h5>${restaurant.title}</h5>
                    <p><b>Cena obroka: </b>${restaurant.cenaObroka}</p>
                    <p><b>Cena doplačila: </b>${restaurant.cenaDoplacila}</p>
                    <p><b>Oddaljenost: </b>${this.formatDistance(calculateDistance)} km <p>
                    <p>${stars} (${starRating})</p>
                </div>`;

			const infowindow = new google.maps.InfoWindow({
				content: contentString
			});

			const restaurantMarker = new google.maps.Marker({
				position: new google.maps.LatLng(restaurant.lat, restaurant.lng),
				map: this.googleMap,
				icon: 'assets/icon/restaurant-custom.png'
			});

			restaurantMarker.addListener('click', (_) => {
				infowindow.open(this.googleMap, restaurantMarker);
			});
		}
	}

	rad(x) {
		return x * Math.PI / 180;
	}

	degreesToRadians(degrees) {
		return degrees * Math.PI / 180;
	}

	getDistance(lat1, lon1, lat2, lon2) {
		const earthRadiusKm = 6371;

		const dLat = this.degreesToRadians(lat2 - lat1);
		const dLon = this.degreesToRadians(lon2 - lon1);

		lat1 = this.degreesToRadians(lat1);
		lat2 = this.degreesToRadians(lat2);

		const a =
			Math.sin(dLat / 2) * Math.sin(dLat / 2) +
			Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
		const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		return earthRadiusKm * c;
	}

	formatDistance(x: number) {
		return parseFloat('' + Math.round(x * 100) / 100).toFixed(2);
	}
}
