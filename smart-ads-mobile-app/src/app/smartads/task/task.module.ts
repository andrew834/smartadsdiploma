import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { TaskPage } from './task.page';
import { PopoverComponent } from './popover/popover.component';
import { CreateComponent } from './create/create.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { UpdateComponent } from './update/update.component';

const routes: Routes = [
	{
		path: '',
		component: TaskPage
	}
];

@NgModule({
	imports: [ CommonModule, IonicModule, RouterModule.forChild(routes), ReactiveFormsModule, FormsModule ],
	declarations: [ TaskPage, PopoverComponent, CreateComponent, UpdateComponent ],
	entryComponents: [ PopoverComponent, CreateComponent, UpdateComponent ]
})
export class TaskPageModule {
	constructor() {}
}
