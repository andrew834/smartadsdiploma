import { Component } from '@angular/core';
import { PopoverController, NavParams } from '@ionic/angular';

@Component({
	templateUrl: './popover.component.html',
	styleUrls: [ './popover.component.scss' ]
})
export class PopoverComponent {
	task = null;
	constructor(private navParams: NavParams, private popoverCtrl: PopoverController) {
		this.task = navParams.get('task');
	}

	onEdit() {
		this.popoverCtrl.dismiss({ e: 'edit', task: this.task });
	}

	onDelete() {
		this.popoverCtrl.dismiss({ e: 'delete', id: this.task._id });
	}
}
