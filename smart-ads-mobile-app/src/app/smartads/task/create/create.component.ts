import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { TaskModel } from '../task.model';
import { AlertController, ModalController, LoadingController } from '@ionic/angular';
import { HttpService } from 'src/app/shared/services/http.service';

@Component({
	templateUrl: './create.component.html',
	styleUrls: [ './create.component.scss' ]
})
export class CreateComponent implements OnInit {
	form: FormGroup;
	constructor(
		private alertCtrl: AlertController,
		private modalCtrl: ModalController,
		private httpService: HttpService,
		private loadingCtrl: LoadingController
	) {}

	ngOnInit() {
		this.form = new FormGroup({
			title: new FormControl(null, {
				updateOn: 'change',
				validators: [
					Validators.required,
					Validators.minLength(3),
					Validators.maxLength(40),
					Validators.pattern('^[a-žA-Ž0-9 ]+$')
				]
			}),
			description: new FormControl(null, {
				updateOn: 'change',
				validators: [ Validators.required, Validators.minLength(0), Validators.maxLength(2000) ]
			}),
			urgency: new FormControl(null, {
				updateOn: 'change',
				validators: [ Validators.required ]
			})
		});
	}

	onCreateTask() {
		if (!this.form.valid) {
			this.onError('Oh ne!', 'Nekatera polja so neustrezno izpolnjena.');
			return;
		}

		const taskData: TaskModel = this.form.value;

		this.loadingCtrl
			.create({
				keyboardClose: true,
				duration: 6000,
				message: 'Kreiranje opravka...'
			})
			.then((loader) => {
				loader.present();
				this.httpService.createTask(taskData).subscribe(
					(serverRes) => {
						loader.dismiss();
						this.modalCtrl.dismiss(serverRes.d);
					},
					(serverErr) => {
						loader.dismiss();
						this.onError(
							'Oh ne!',
							serverErr.error && serverErr.error.m
								? serverErr.error.m
								: 'Nekaj je šlo narobe. Poiskusite kasneje.'
						);
					}
				);
			});
	}

	onError(header: string, message: string) {
		this.alertCtrl
			.create({
				header,
				message,
				buttons: [ 'Zapri' ]
			})
			.then((alert) => {
				alert.present();
			});
	}

	closeModal() {
		this.modalCtrl.dismiss(null);
	}
}
