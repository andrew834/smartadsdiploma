import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { PopoverController, ModalController, AlertController, LoadingController } from '@ionic/angular';
import { PopoverComponent } from './popover/popover.component';
import { CreateComponent } from './create/create.component';
import { HttpService } from 'src/app/shared/services/http.service';
import { UpdateComponent } from './update/update.component';
import { TaskModel } from './task.model';
import { ActionsService } from 'src/app/shared/services/actions.service';
@Component({
	templateUrl: './task.page.html',
	styleUrls: [ './task.page.scss' ]
})
export class TaskPage implements OnInit {
	taskMode: 'getting' | 'gotten' = 'getting';
	constructor(
		public popoverController: PopoverController,
		private modalCtrl: ModalController,
		private alertCtrl: AlertController,
		private httpService: HttpService,
		private loadingCtrl: LoadingController,
		private changeDetector: ChangeDetectorRef,
		private actionsService: ActionsService
	) {}

	myTasks = [];

	ngOnInit() {
		this.httpService.getMyTasks().subscribe(
			(serverRes) => {
				this.taskMode = 'gotten';
				this.myTasks = serverRes.t;
				this.changeDetector.detectChanges();
			},
			(serverErr) => {
				console.log(serverErr);
				this.taskMode = 'gotten';
				this.changeDetector.detectChanges();
			}
		);
	}

	editTask(taskData: any, event) {
		this.popoverController
			.create({
				component: PopoverComponent,
				translucent: true,
				event,
				componentProps: {
					task: taskData
				}
			})
			.then((popoverEl) => {
				popoverEl.present();
				return popoverEl.onDidDismiss().then((returnData) => {
					if (returnData.data != null) {
						switch (returnData.data.e) {
							case 'delete':
								this.onRequestDelete(returnData.data.id);
								break;
							case 'edit':
								this.onRequestEdit(returnData.data.task);
								break;
						}
					}
				});
			});
	}

	newTask() {
		this.modalCtrl
			.create({
				component: CreateComponent
			})
			.then((modalEl) => {
				modalEl.present();

				return modalEl.onDidDismiss().then((modalData) => {
					if (modalData.data != null) {
						this.myTasks.push(modalData.data);
						this.changeDetector.detectChanges();
						this.actionsService.actionFiredShowAd();
					}
				});
			});
	}

	onRequestDelete(taskId) {
		this.alertCtrl
			.create({
				header: 'Brisanje opravka',
				message: 'Ali ste prepričani, da želite izbrisati opravek?',
				buttons: [
					{
						text: 'Prekini',
						role: 'cancel'
					},
					{
						text: 'Izbriši',
						handler: (_) => {
							this.alertCtrl.dismiss();
							this.loadingCtrl
								.create({ message: 'Brisanje opravka...', duration: 6000 })
								.then((loader) => {
									loader.present();
									this.httpService.deleteTask(taskId).subscribe(
										(serverRes) => {
											loader.dismiss();
											this.removeTaskFromArray(taskId);
											this.actionsService.actionFiredShowAd()
										},
										(serverErr) => {
											console.log(serverErr);
											loader.dismiss();
											this.onError(
												'Prišlo je do napake',
												serverErr.error && serverErr.error.m
													? serverErr.error.m
													: 'Nekaj je šlo narobe. Poiskusite kasneje.'
											);
										}
									);
								});
						}
					}
				]
			})
			.then((alertEl) => {
				alertEl.present();
			});
	}

	removeTaskFromArray(taskId: string) {
		for (let i = 0; i < this.myTasks.length; i++) {
			if (this.myTasks[i]._id === taskId) {
				this.myTasks.splice(i, 1);
				this.changeDetector.detectChanges();
				break;
			}
		}
	}

	onRequestEdit(task) {
		this.modalCtrl
			.create({
				component: UpdateComponent,
				componentProps: {
					title: task.title,
					description: task.description,
					urgency: task.urgency,
					_id: task._id
				}
			})
			.then((modalEl) => {
				modalEl.present();
				return modalEl.onDidDismiss().then((modalData) => {
					if (modalData.data != null && modalData.data.d != null) {
						this.updateTaskInArray(modalData.data.d);
						this.actionsService.actionFiredShowAd()
					}
				});
			});
	}

	updateTaskInArray(taskModel: TaskModel) {
		for (let i = 0; i < this.myTasks.length; i++) {
			if (this.myTasks[i]._id === taskModel._id) {
				this.myTasks[i] = taskModel;
				this.changeDetector.detectChanges();
				break;
			}
		}
	}

	onError(header: string, message: string) {
		this.alertCtrl
			.create({
				header,
				message,
				buttons: [ 'Zapri' ]
			})
			.then((alert) => {
				alert.present();
			});
	}

	onConvertColor(colorVal) {
		switch (colorVal) {
			case '#F25454':
				return 'Obvezno';
			case '#FFCF68':
				return 'Pomembno';
			case '#AFE067':
				return 'Nepomembno';
			case '#4C8DFF':
				return 'Opcijsko';
		}

		return 'Opcijsko';
	}
}
