export interface TaskModel {
	date: Date;
	title: string;
	description: string;
	urgency: string;
	_id?: string;
}
