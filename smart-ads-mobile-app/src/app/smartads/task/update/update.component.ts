import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { TaskModel } from '../task.model';
import { AlertController, ModalController, LoadingController } from '@ionic/angular';
import { HttpService } from 'src/app/shared/services/http.service';

@Component({
	templateUrl: './update.component.html',
	styleUrls: [ './update.component.scss' ]
})
export class UpdateComponent implements OnInit {
	form: FormGroup;

	@Input() _id;
	@Input() title;
	@Input() description;
	@Input() urgency;

	constructor(
		private alertCtrl: AlertController,
		private modalCtrl: ModalController,
		private httpService: HttpService,
		private loadingCtrl: LoadingController
	) {}

	ngOnInit() {
		this.form = new FormGroup({
			title: new FormControl(this.title, {
				updateOn: 'change',
				validators: [
					Validators.required,
					Validators.minLength(3),
					Validators.maxLength(40),
					Validators.pattern('^[a-žA-Ž0-9 ]+$')
				]
			}),
			description: new FormControl(this.description, {
				updateOn: 'change',
				validators: [ Validators.required, Validators.minLength(0), Validators.maxLength(2000) ]
			}),
			urgency: new FormControl(this.urgency, {
				updateOn: 'change',
				validators: [ Validators.required ]
			})
		});
	}

	onUpdateTask() {
		if (!this.form.valid) {
			this.onError('Oh ne!', 'Nekatera polja so neustrezno izpolnjena.');
			return;
		}

		const taskData: TaskModel = this.form.value;

		this.loadingCtrl
			.create({
				keyboardClose: true,
				duration: 6000,
				message: 'Posodabljanje opravka...'
			})
			.then((loader) => {
				loader.present();
				this.httpService.updateTask(this._id, taskData).subscribe(
					() => {
						loader.dismiss();
						this.modalCtrl.dismiss({
							d: {
								title: taskData.title,
								description: taskData.description,
								urgency: taskData.urgency,
								_id: this._id,
								date: new Date().toISOString()
							}
						});
					},
					(serverErr) => {
						loader.dismiss();
						this.onError(
							'Oh ne!',
							serverErr.error && serverErr.error.m
								? serverErr.error.m
								: 'Nekaj je šlo narobe. Poiskusite kasneje.'
						);
					}
				);
			});
	}

	onError(header: string, message: string) {
		this.alertCtrl
			.create({
				header,
				message,
				buttons: [ 'Zapri' ]
			})
			.then((alert) => {
				alert.present();
			});
	}

	closeModal() {
		this.modalCtrl.dismiss(null);
	}
}
