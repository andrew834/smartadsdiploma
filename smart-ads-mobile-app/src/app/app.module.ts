import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { HttpClientModule } from '@angular/common/http';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthService } from './auth/auth.service';
import { MenuService } from './menu.service';
import { HttpService } from './shared/services/http.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DatePipe } from '@angular/common';
import { ContextService } from './shared/services/context.service';
import { BatteryStatus } from '@ionic-native/battery-status/ngx';
import { ActionsService } from './shared/services/actions.service';
import { AdsComponent } from './smartads/ads/ads.component';
import { FeedbackComponent } from './smartads/ads/feedback/feedback.component';

@NgModule({
	declarations: [ AppComponent, FeedbackComponent, AdsComponent ],
	entryComponents: [FeedbackComponent, AdsComponent],
	imports: [ BrowserModule, IonicModule.forRoot(), HttpClientModule, BrowserAnimationsModule, AppRoutingModule ],
	providers: [
		StatusBar,
		SplashScreen,
		BatteryStatus,
		{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
		AuthService,
		MenuService,
		HttpService,
		DatePipe,
		ContextService,
		ActionsService
	],
	exports: [FeedbackComponent, AdsComponent],
	bootstrap: [ AppComponent ]
})
export class AppModule {}
