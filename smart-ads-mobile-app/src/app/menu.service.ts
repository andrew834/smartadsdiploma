import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
	providedIn: 'root'
})
export class MenuService {
	private _menu = new BehaviorSubject<boolean>(false);

	get menuState() {
		return this._menu.asObservable().pipe(
			map((menu) => {
				return menu;
			})
		);
	}

	switchMenuMode(switchMode: boolean) {
		this._menu.next(switchMode);
	}
}
