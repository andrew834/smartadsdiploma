import { Component, OnDestroy, OnInit } from '@angular/core';

import { Platform, NavController, LoadingController, AlertController, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { AuthService } from './auth/auth.service';
import { MenuService } from './menu.service';
import { Plugins } from '@capacitor/core';
import { UserModel } from './auth/user.model';
import * as jwt_decode from 'jwt-decode';
import { Router } from '@angular/router';
import { ContextService } from './shared/services/context.service';
import { take } from 'rxjs/operators';
import { Subscription } from 'rxjs';

const { Geolocation, Storage, StatusBar } = Plugins;

@Component({
	selector: 'app-root',
	templateUrl: 'app.component.html',
	styleUrls: [ 'app.component.scss' ]
})
export class AppComponent implements OnInit,OnDestroy {
	constructor(
		private platform: Platform,
		private splashScreen: SplashScreen,
		private authService: AuthService,
		private navCtrl: NavController,
		private menuService: MenuService,
		private router: Router,
		private loadingCtrl: LoadingController,
		private contextService: ContextService,
		private alertCtrl: AlertController
	) {
		this.initializeApp();
	}

	private NAVIGATION_URL = '/smartads/tabs/news';

	pauseSub: Subscription;
	resumeSub: Subscription;

	initializeApp() {
		this.platform.ready().then(() => {
			//console.log("OnInit APP");
			
			Geolocation.requestPermissions().then(res => {
				this.postRequestPermission();
			}).catch(err => {
				this.alertCtrl.create({
					header: "Lokacija zavrnjena",
					message: "Nekatere funkcionalnosti morda ne bodo pravilno delovale.",
					buttons: ["Razumem"]
				}).then(alertEl => {
					alertEl.present();
					return alertEl.onDidDismiss().then(_ => {
						this.postRequestPermission();
					})
				});
			});
		});
	}

	ngOnInit() {}

	postRequestPermission() {
		this.pauseSub = this.platform.pause.subscribe(() => {
			//console.log("PAUSING ACTION");
			this.contextService.destroyActivitiesListener();
		});

		this.resumeSub = this.platform.resume.subscribe(() => {
			//console.log("RESUMING ACTION");

			this.contextService.initializeService();
			Storage.get({ key: 'smartads-data' }).then((data) => {
				let storageData = JSON.parse(data.value);
				if (storageData != null) {
					if (!storageData.tokenExperationDate || new Date(storageData.tokenExperationDate) <= new Date()) {
						return;
					} else {
						this.authService.onRefreshToken(storageData._token).subscribe(
							(serverRes) => {
								if (serverRes && serverRes.t) {
									const decodedToken = jwt_decode(serverRes.t);
									this.authService.setUserData(
										new UserModel(
											decodedToken.id,
											decodedToken.u,
											serverRes.t,
											decodedToken.g,
											decodedToken.b,
											new Date(decodedToken.exp * 1000)
										)
									);
								}
							},
							(serverErr) => {
								this.navCtrl.navigateRoot(["/auth"]);
								console.log(serverErr);
							}
						);
					}
				}
			});	
		});


		Storage.get({ key: 'smartads-data' }).then((data) => {	
				this.splashScreen.hide();

				if (this.platform.is('mobile')) {
					screen.orientation.lock('portrait');
				}

				let storageData = JSON.parse(data.value);
				if (storageData != null) {
					if (!storageData.tokenExperationDate || new Date(storageData.tokenExperationDate) <= new Date()) {
						return;
					} else {
						this.loadingCtrl
							.create({
								message: `Avtomatska prijava...`
							})
							.then((loader) => {
								loader.present();
								this.authService.onRefreshToken(storageData._token).subscribe(
									(serverRes) => {
										if (serverRes && serverRes.t) {
											const decodedToken = jwt_decode(serverRes.t);
											this.authService.setUserData(
												new UserModel(
													decodedToken.id,
													decodedToken.u,
													serverRes.t,
													decodedToken.g,
													decodedToken.b,
													new Date(decodedToken.exp * 1000)
												)
											);
											setTimeout((_) => {
												loader.dismiss();
												this.router.navigateByUrl(this.NAVIGATION_URL, { replaceUrl: true });
											}, 1000);
										}
									},
									(serverErr) => {
										loader.dismiss();
										console.log(serverErr);
									}
								);
							});
					}
				}
			});	

		this.contextService.initializeService();
	}

	onLogout() {
		this.authService.onLogout().subscribe((_) => {
			this.navCtrl.navigateRoot([ '/auth' ]);
		});
	}

	currentMode(mode) {
		this.menuService.switchMenuMode(mode);
	}

	ngOnDestroy() {
		if (this.resumeSub) {
			this.resumeSub.unsubscribe();
		}

		if (this.pauseSub) {
			this.pauseSub.unsubscribe();
		}

		this.contextService.destroyActivitiesListener();
	}
}
