import { Component, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { MenuController, AlertController } from '@ionic/angular';
import { MenuService } from '../menu.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
	selector: 'app-auth',
	templateUrl: './auth.page.html',
	styleUrls: [ './auth.page.scss' ]
})
export class AuthPage implements OnDestroy {
	currentMode = 'login';
	currentTitle = 'Prijava';
	menuSub: Subscription;
	private NAVIGATION_URL = '/smartads/tabs/news';

	constructor(
		private menuCtrl: MenuController,
		private menuService: MenuService,
		private chandeDetector: ChangeDetectorRef,
		private router: Router,
		private alertCtrl: AlertController
	) {}

	onSwapAuth() {
		this.currentTitle = this.currentMode === 'login' ? 'Registracija' : 'Prijava';
		this.currentMode = this.currentMode === 'login' ? 'register' : 'login';
		this.chandeDetector.detectChanges();
	}

	ionViewDidEnter() {
		this.menuSub = this.menuService.menuState.subscribe((state) => {
			if (!state) {
				this.menuCtrl.swipeGesture(false);
			}
		});
	}

	ionViewDidLeave() {
		if (this.menuSub) {
			this.menuSub.unsubscribe();
		}
	}

	onRecieveEvent(data) {
		this.currentMode = 'tutorial';
		this.currentTitle = 'O aplikaciji';
	}

	onEndTutorial() {
		this.alertCtrl.create({
			header: "Lokacija",
			message: "Zaželjeno je, da imate v času nagradne igre prižgano lokacijo na telefonu.",
			buttons: ["Razumem"]
		}).then(alertEl => {
			alertEl.present();	
			return alertEl.onDidDismiss().then(_ => {
				this.router.navigateByUrl(this.NAVIGATION_URL, { replaceUrl: true });
			});
		});
	}

	ngOnDestroy() {
		if (this.menuSub) {
			this.menuSub.unsubscribe();
		}
	}
}
