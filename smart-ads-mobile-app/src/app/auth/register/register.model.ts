export interface RegisterModel {
	u: string;
	p: string;
	rP: string;
	b: string;
	g: 'm' | 'f' | 'o';
}
