import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AlertController, ModalController } from '@ionic/angular';

import { Plugins } from '@capacitor/core';
import { ContextService } from 'src/app/shared/services/context.service';

const { Storage } = Plugins;

@Component({
	templateUrl: './location.component.html',
	styleUrls: [ './location.component.scss' ]
})
export class LocationComponent implements OnInit {
	constructor(private contextService: ContextService, private alertCtrl: AlertController, private modalCtrl: ModalController) {}

    optionsAutocomplete = {
        types: ['address'],
        componentRestrictions: {country: "si"}
    };
      
    form: FormGroup;

    inputHome: HTMLInputElement;
    inputWork: HTMLInputElement;
    autocompleteHome: google.maps.places.Autocomplete;
    autocompleteWork: google.maps.places.Autocomplete;

    ionViewDidEnter() {
        this.inputHome = document.querySelector('#home input') as HTMLInputElement;
        this.inputWork = document.querySelector('#work input') as HTMLInputElement;

        this.autocompleteHome = new google.maps.places.Autocomplete(this.inputHome, this.optionsAutocomplete);
        this.autocompleteWork = new google.maps.places.Autocomplete(this.inputWork, this.optionsAutocomplete); 
    }

	ngOnInit() {
		this.form = new FormGroup({
			b: new FormControl(null, {
				updateOn: 'change',
				validators: [
                    Validators.required
                ]
			}),
			d: new FormControl(null, {
				updateOn: 'change',
				validators: [ Validators.required ]
			})
		});
    }
    
    onRegister() {
        if (!this.form.valid) {
			this.onError('Oh ne!', 'Nekatera polja so neustrezno izpolnjena.');
			return;
        }
    
        try {
            const home = this.autocompleteHome.getPlace().geometry.location;
            const work = this.autocompleteWork.getPlace().geometry.location; 

            const storeHome = {
                lat: home.lat(),
                lng: home.lng()
            };

            const storeWork = {
                lat: work.lat(),
                lng: work.lng()
            };

            const storageObj = {
                work: storeWork,
                home: storeHome
            };

            this.contextService.setContextUserLocationStorage(storeHome, storeWork);

            Storage.set({key: 'userLocation', value: JSON.stringify(storageObj)}).then(_ => {
                this.modalCtrl.dismiss({status: 'OK'});
            });
        } catch (err) {
            this.alertCtrl.create({
                header: "Oh ne!",
                message: "Prišlo je do napake, prosimo preverite vnosna polja in ponovno poiskusite.",
                buttons: ["Razumem"]
            }).then(alertEl => {
                alertEl.present();
            });
        }
    }

    onError(header: string, message: string) {
		this.alertCtrl
			.create({
				header,
				message,
				buttons: [ 'Zapri' ]
			})
			.then((alert) => {
				alert.present();
			});
	}
}
