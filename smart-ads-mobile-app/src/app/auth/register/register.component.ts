import { Component, OnInit, ChangeDetectorRef, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AlertController, LoadingController, ModalController } from '@ionic/angular';

import { AuthService } from '../auth.service';
import { RegisterModel } from './register.model';
import { UserModel } from '../user.model';

import * as jwt_decode from 'jwt-decode';
import { isValidDate } from './register.validator';
import { Plugins } from '@capacitor/core';
import { LocationComponent } from './locationPicker/location.component';
import { ContextService } from 'src/app/shared/services/context.service';

const { Storage } = Plugins;

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: [ './register.component.scss' ]
})
export class RegisterComponent implements OnInit {
	form: FormGroup;
	isLoadingData = false;
	@Output('tutorial') tutorialEmit = new EventEmitter();

	constructor(
		private authService: AuthService,
		private loadingCtrl: LoadingController,
		private router: Router,
		private alertCtrl: AlertController,
		private changeDetector: ChangeDetectorRef,
		private modalCtrl: ModalController,
		private contextService: ContextService
	) {}

	ngOnInit() {
		this.form = new FormGroup({
			u: new FormControl(null, {
				updateOn: 'change',
				validators: [
					Validators.required,
					Validators.minLength(3),
					Validators.maxLength(20),
					Validators.pattern('^[a-zA-Z0-9]+$')
				]
			}),
			p: new FormControl(null, {
				updateOn: 'change',
				validators: [ Validators.required, Validators.minLength(6), Validators.maxLength(20) ]
			}),
			rP: new FormControl(null, {
				updateOn: 'change',
				validators: [ Validators.required, Validators.minLength(6), Validators.maxLength(20) ]
			}),
			b: new FormControl(null, {
				updateOn: 'change',
				validators: [ Validators.required, isValidDate ]
			}),
			g: new FormControl('m', {
				updateOn: 'change',
				validators: [ Validators.required ]
			})
		});
	}

	onRegister() {
		if (!this.form.valid) {
			this.onError('Oh ne!', 'Nekatera polja so neustrezno izpolnjena.');
			return;
		}

		const registerData: RegisterModel = this.form.value;

		if (registerData.p !== registerData.rP) {
			this.onError('Oh ne!', 'Gesli se ne ujemata.');
			return;
		}

		this.alertCtrl.create({
			header: "Način pridobivanja lokacije",
			message: "Ali dovolite aplikaciji beleženje lokacije?",
			buttons: [{
				text: "Ne",
				handler: () => {
					this.createLocationMethod("user").finally(() => {
						this.modalSetup(registerData);
					});
				}
			},{
				text: "Da",
				handler: () => {
					this.createLocationMethod("ai").finally(() => {
						this.completeRegistration(registerData);
					});
				}
			}]
		}).then(alertEl => {
			alertEl.present();
		});
	}

	createLocationMethod(method: "user" | "ai"){
		return Storage.set({key: "locationMethod", value: method}).then(_ => {
			if(method == "ai"){
				return Storage.set({key: "shouldStillSample", value: "true"}).then(_ => {
					Storage.set({ key: 'startedSampling', value: new Date().toISOString() });
				});
			}
		});
	}

	modalSetup(registerData) {
		this.modalCtrl.create({
			component: LocationComponent
		}).then(modalEl => {
			modalEl.present();
			return modalEl.onDidDismiss().then(returnData => {
				if(returnData.data != null) {
					this.completeRegistration(registerData);
				}
			});
		});
	}

	completeRegistration(registerData) {
		this.loadingCtrl
		.create({
			keyboardClose: true,
			duration: 6000,
			message: 'Ustvarjanje računa...'
		})
		.then((loader) => {
			loader.present();
			setTimeout((_) => {
				this.authService.onRegister(registerData).subscribe(
					(serverRes) => {
						loader.dismiss();
						if (serverRes && serverRes.t) {
							const decodedToken = jwt_decode(serverRes.t);
							this.authService.setUserData(
								new UserModel(
									decodedToken.id,
									decodedToken.u,
									serverRes.t,
									decodedToken.g,
									decodedToken.b,
									new Date(decodedToken.exp * 1000)
								)
							);
							
							this.contextService.forceUpdateLocation();
							this.tutorialEmit.emit('tutorial');
						} else {
							this.onError(
								'Oh ne!',
								serverRes.m ? serverRes.m : 'Nekaj je šlo narobe. Poiskusite kasneje.'
							);
						}
					},
					(serverErr) => {
						loader.dismiss();
						this.onError(
							'Oh ne!',
							serverErr.error && serverErr.error.m
								? serverErr.error.m
								: 'Nekaj je šlo narobe. Poiskusite kasneje.'
						);
					}
				);
			}, 750);
		});
	}

	onError(header: string, message: string) {
		this.alertCtrl
			.create({
				header,
				message,
				buttons: [ 'Zapri' ]
			})
			.then((alert) => {
				alert.present();
			});
	}
}
