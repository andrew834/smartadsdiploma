import {  FormControl } from '@angular/forms';

export function isValidDate(control: FormControl) {
	try {
		if (!control.value) {
			return { d: true };
		}

		const timestamp = Date.parse(control.value);
		const MIN_AGE_LIMIT = 1000 * 60 * 60 * 24 * 365 * 3;
		if (!isNaN(timestamp)) {
			if (new Date().getTime() - timestamp > MIN_AGE_LIMIT) {
				return null;
			}
			throw 'User is under the age of 3.';
		}
		throw 'Invalid date.';
	} catch (e) { }

	return { d: true };
}
