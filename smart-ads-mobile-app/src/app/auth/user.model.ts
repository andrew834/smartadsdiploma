export class UserModel {
	constructor(
		public id: string,
		public username: string,
		private _token: string,
		public gender: 'm' | 'f' | 'o',
		public birthday: Date,
		private tokenExperationDate: Date
	) {}

	get token() {
		if (!this.tokenExperationDate || new Date(this.tokenExperationDate) <= new Date()) {
			return null;
		}

		return this._token;
	}
}
