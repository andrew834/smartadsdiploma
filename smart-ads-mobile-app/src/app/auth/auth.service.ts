import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { map, take, tap } from 'rxjs/operators';

import { UserModel } from './user.model';
import { RegisterModel } from './register/register.model';
import { LoginModel } from './login/login.model';
import { Plugins } from '@capacitor/core';

const { Storage } = Plugins;

@Injectable({
	providedIn: 'root'
})
export class AuthService {
	private API_URL = 'http://163.172.169.249:9080';
	private _user = new BehaviorSubject<UserModel>(null);

	constructor(private http: HttpClient) {}

	get isUserAuthenticated() {
		return this._user.asObservable().pipe(
			map((user) => {
				return user ? !!user.token : false;
			})
		);
	}

	get user() {
		return this._user.asObservable().pipe(
			map((user) => {
				return user ? user : null;
			})
		);
	}

	setUserData(user: UserModel) {
		Storage.set({ key: 'smartads-data', value: JSON.stringify(user) });
		this._user.next(user);
	}

	onRegister(registerData: RegisterModel) {
		return this.http.post<any>(`${this.API_URL}/user/register`, registerData);
	}

	onRefreshToken(token: string) {
		const headers = new HttpHeaders({
			'X-Access-Token': token
		});

		return this.http.get<any>(`${this.API_URL}/user/refresh`, { headers });
	}

	onLogin(loginData: LoginModel) {
		return this.http.post<any>(`${this.API_URL}/user/login`, loginData);
	}

	onLogout() {
		return this.user.pipe(
			take(1),
			tap((_) => {
				this.setUserData(null);
			})
		);
	}
}
