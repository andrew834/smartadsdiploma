import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { HttpClientModule } from '@angular/common/http';

import { AuthPage } from './auth.page';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { LocationComponent } from './register/locationPicker/location.component';

const routes: Routes = [
	{
		path: '',
		component: AuthPage
	}
];

@NgModule({
	imports: [ CommonModule, ReactiveFormsModule, IonicModule, HttpClientModule, RouterModule.forChild(routes) ],
	declarations: [ AuthPage, RegisterComponent, LoginComponent, LocationComponent ],
	exports: [LocationComponent],
	entryComponents: [LocationComponent]
})
export class AuthPageModule {}
