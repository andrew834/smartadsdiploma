import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LoadingController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

import { AuthService } from '../auth.service';

import { LoginModel } from './login.model';
import { UserModel } from '../user.model';

import * as jwt_decode from 'jwt-decode';
import { ContextService } from 'src/app/shared/services/context.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: [ './login.component.scss' ]
})
export class LoginComponent implements OnInit {
	private NAVIGATION_URL = '/smartads/tabs/news';
	form: FormGroup;

	constructor(
		private authService: AuthService,
		private loadingCtrl: LoadingController,
		private router: Router,
		private alertCtrl: AlertController,
		private contextService: ContextService
	) {}

	ngOnInit() {
		this.form = new FormGroup({
			u: new FormControl(null, {
				updateOn: 'change',
				validators: [
					Validators.required,
					Validators.minLength(3),
					Validators.maxLength(20),
					Validators.pattern('^[a-zA-Z0-9]+$')
				]
			}),
			p: new FormControl(null, {
				updateOn: 'change',
				validators: [ Validators.required, Validators.minLength(6), Validators.maxLength(20) ]
			})
		});
	}

	onLogin() {
		if (!this.form.valid) {
			this.onError('Oh ne!', 'Nekatera polja so neustrezno izpolnjena.');
			return;
		}

		const loginData: LoginModel = this.form.value;

		this.loadingCtrl
			.create({
				keyboardClose: true,
				duration: 6000,
				message: 'Prijava...'
			})
			.then((loader) => {
				loader.present();
				setTimeout((_) => {
					this.authService.onLogin(loginData).subscribe(
						(serverRes) => {
							loader.dismiss();
							if (serverRes && serverRes.t) {
								const decodedToken = jwt_decode(serverRes.t);
								this.authService.setUserData(
									new UserModel(
										decodedToken.id,
										decodedToken.u,
										serverRes.t,
										decodedToken.g,
										decodedToken.b,
										new Date(decodedToken.exp * 1000)
									)
								);
								
								this.contextService.forceUpdateLocation();
								this.router.navigateByUrl(this.NAVIGATION_URL, { replaceUrl: true });
							} else {
								this.onError(
									'Oh ne!',
									serverRes.m ? serverRes.m : 'Nekaj je šlo narobe. Poiskusite kasneje.'
								);
							}
						},
						(serverErr) => {
							loader.dismiss();
							console.log(serverErr);
							this.onError(
								'Prišlo je do napake',
								serverErr.error && serverErr.error.m
									? serverErr.error.m
									: 'Nekaj je šlo narobe. Poiskusite kasneje.'
							);
						}
					);
				}, 750);
			});
	}

	onError(header: string, message: string) {
		this.alertCtrl
			.create({
				header,
				message,
				buttons: [ 'Zapri' ]
			})
			.then((alert) => {
				alert.present();
			});
	}
}
