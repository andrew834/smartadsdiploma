import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { take, switchMap, tap, map } from 'rxjs/operators';
import { Plugins, PluginListenerHandle, Geolocation, FilesystemDirectory, FilesystemEncoding } from '@capacitor/core';
import { Platform } from '@ionic/angular';
import { BehaviorSubject, Subscription } from 'rxjs';
import { ContextModel } from './context.model';
import { BatteryStatus } from '@ionic-native/battery-status/ngx';
import * as clustering from 'density-clustering';
import { HttpService } from './http.service';

const { SmartAdsPlugin, Network, Device, Storage, Filesystem } = Plugins;

@Injectable()
export class ContextService {
	private API_URL = 'http://163.172.169.249:9080';
	private GLOBAL_RADIUS = 500; // 500m
	private NIGHT_HOUR = {
		START: 0,
		END: 6
	};

	private DAY_HOUR = {
		START: 9,
		END: 17
	};

	private DBSCAN_MINPOINTS = 15;
	private DBSCAN_RADIUS = 0.004; // 0.0025 -> x: 0.1931m y: 0.27975m
	private CLUSTERING_MIN_TIME = 1000 * 60 * 60 * 24 * 7;

	private _context = new BehaviorSubject<ContextModel>(
		new ContextModel(
			{ name: 'STILL', confidence: 100 },
			{ connected: false, connectionType: 'none' },
			{ status: 'MEDIUM', level: 0.5, isPlugged: false },
			{ status: 'LOW', value: 127 },
			{ ...this.returnCurrentDate() },
			{
				work: { lat: 46.05038, lng: 14.46848 },
				home: { lat: 0.0, lng: 0.0 },
				currentLocation: { status: 'OTHER', lat: 90.0, lng: 90.0 }
			}
		)
	);

	private networkListener: PluginListenerHandle;

	private batterySubscription: Subscription;

	private periodicGetBrightnessInfo;
	private BRIGHTNESS_GET_PERIOD_IN_MILIS = 1000 * 60 * 5; // 5 min

	private periodicGetUserLocation;
	private USER_LOCATION_GET_PERIOD_IN_MILIS = 1000 * 60 * 3; // 3 min

	constructor(
		private http: HttpClient,
		private platform: Platform,
		private batteryStatus: BatteryStatus,
		private httpService: HttpService
	) {}

	initializeService() {
		if (this.platform.is('mobile')) {
			this.initializeActivityPlugin();
			this.initializeNetwork();
			this.initializeBatteryStatus();
			this.initializeBrightness();
			this.checkForLocationSampling();
			this.initializeLocation();
		}
	}

	forceUpdateLocation() {
		navigator.geolocation.getCurrentPosition((location) => {
			this.setContextUserLocation({ lat: location.coords.latitude, lng: location.coords.longitude });
		}, (err) => {
			console.log(err);
		}, { timeout: 10000, maximumAge: 10000, enableHighAccuracy: true });

		/*
		Geolocation.getCurrentPosition({
			enableHighAccuracy: true,
			timeout: 30000,
			maximumAge: 30000
		}).then((location) => {
			this.setContextUserLocation({ lat: location.coords.latitude, lng: location.coords.longitude });
		});*/
	}

	checkForLocationSampling() {
		Filesystem.readFile({
			path: 'smart-ads.csv',
			directory: FilesystemDirectory.External,
			encoding: FilesystemEncoding.UTF8
		})
			.then((fileData) => {
				this.locationSampling(fileData.data);
			})
			.catch((err) => {
				this.locationSampling(null);
			});
	}

	locationSampling(fileData) {
		Storage.get({ key: 'locationMethod' })
			.then((data) => {
				if (data.value == 'ai') {
					Storage.get({ key: 'shouldStillSample' }).then((shouldStillSample) => {
						if (shouldStillSample.value == 'true') {
							Storage.get({ key: 'startedSampling' }).then((startDate) => {
								const date = new Date(startDate.value);
								if (new Date().getTime() - date.getTime() > this.CLUSTERING_MIN_TIME) {
									if (data != null) {
										let { dbScanPoints, locationPoints } = this.parseFileData(fileData);
										if (locationPoints.length > 50) {
											let dbscan = new clustering.DBSCAN();
											let clusters = dbscan.run(
												dbScanPoints,
												this.DBSCAN_RADIUS,
												this.DBSCAN_MINPOINTS
											);

											if (!(clusters == null || clusters.length < 2)) {
												const clusteringResult = this.extractHomeAndWork(
													clusters,
													locationPoints
												);

												return Storage.set({ key: 'shouldStillSample', value: 'false' })
													.then((_) => {
														return Storage.set({
															key: 'userLocation',
															value: JSON.stringify(clusteringResult)
														});
													})
													.then((_) => {
														this.updateServerData(
															clusteringResult.home,
															clusteringResult.work
														);

														this.setContextUserLocationStorage(
															clusteringResult.home,
															clusteringResult.work
														);
														SmartAdsPlugin.disableLocationManager();
													});
											}
										}
									}
								}
								SmartAdsPlugin.enableLocationManager().then(() => {
									this.getLocationFromStorage();
								});
							});
						} else if (shouldStillSample.value == null) {
							Storage.set({ key: 'shouldStillSample', value: 'true' }).then((_) => {
								Storage.set({ key: 'startedSampling', value: new Date().toISOString() }).then((_) => {
									SmartAdsPlugin.enableLocationManager().then(() => {
										this.getLocationFromStorage();
									});
								});
							});
						} else {
							SmartAdsPlugin.disableLocationManager().then((_) => {
								this.getLocationFromStorage();
							});
						}
					});
				} else if (data.value == 'user') {
					SmartAdsPlugin.disableLocationManager().then((_) => {
						this.getLocationFromStorage();
					});
				} else {
					Storage.set({ key: 'locationMethod', value: 'ai' }).then(() => {
						Storage.set({ key: 'shouldStillSample', value: 'true' }).then((_) => {
							Storage.set({ key: 'startedSampling', value: new Date().toISOString() }).then((_) => {
								SmartAdsPlugin.enableLocationManager();
							});
						});
					});
				}
			})
			.catch((err) => {
				console.log(err);
				this.getLocationFromStorage();
			});
	}

	updateServerData(home, work) {
		Storage.get({ key: 'smart-ads-locations-cache' }).then((storedData) => {
			let parsedData = JSON.parse(storedData.value);

			if (parsedData != null) {
				let serverData = [];
				for (let i = 0; i < parsedData.length; i++) {
					let labledData = this.labelUserLocation(
						parsedData[i].location.lat,
						parsedData[i].location.lng,
						home,
						work
					);

					serverData.push({ id: parsedData[i].id, location: labledData });
				}

				Storage.set({ key: 'smart-ads-locations-labled', value: JSON.stringify(serverData) });
				Storage.remove({ key: 'smart-ads-locations-cache' });
			}
		});
	}

	extractHomeAndWork(clusters, locationPoints) {
		if (clusters.length == 2) {
			let c0AtNight = this.betweenTwoDates(
				clusters[0],
				locationPoints,
				this.NIGHT_HOUR.START,
				this.NIGHT_HOUR.END
			);
			let c1AtNight = this.betweenTwoDates(
				clusters[1],
				locationPoints,
				this.NIGHT_HOUR.START,
				this.NIGHT_HOUR.END
			);

			if (c0AtNight > c1AtNight) {
				return { home: this.cToP(clusters[0], locationPoints), work: this.cToP(clusters[1], locationPoints) };
			} else {
				return { home: this.cToP(clusters[1], locationPoints), work: this.cToP(clusters[0], locationPoints) };
			}
		} else {
			let bestAtNight = { c: 0, num: 0 };

			for (let i = 0; i < clusters.length; i++) {
				let cAtNight = this.betweenTwoDates(
					clusters[i],
					locationPoints,
					this.NIGHT_HOUR.START,
					this.NIGHT_HOUR.END
				);
				if (cAtNight > bestAtNight.num) {
					bestAtNight = { c: i, num: cAtNight };
				}
			}

			const homeCluster = clusters.splice(bestAtNight.c, 1)[0];

			const c0AtDay = this.bestClusterAt(clusters, locationPoints, this.DAY_HOUR.START, this.DAY_HOUR.END);
			const cluster0 = clusters.splice(c0AtDay.c, 1)[0];

			const c1AtDay = this.bestClusterAt(clusters, locationPoints, this.DAY_HOUR.START, this.DAY_HOUR.END);
			const cluster1 = clusters.splice(c1AtDay.c, 1)[0];

			const c0AtNight = this.betweenTwoDates(
				cluster0,
				locationPoints,
				this.NIGHT_HOUR.START,
				this.NIGHT_HOUR.END
			);
			const c1AtNight = this.betweenTwoDates(
				cluster1,
				locationPoints,
				this.NIGHT_HOUR.START,
				this.NIGHT_HOUR.END
			);

			if (c0AtNight > c1AtNight) {
				return { home: this.cToP(homeCluster, locationPoints), work: this.cToP(cluster1, locationPoints) };
			} else {
				return { home: this.cToP(homeCluster, locationPoints), work: this.cToP(cluster0, locationPoints) };
			}
		}
	}

	cToP(cluster, locationPoints) {
		let points = [];

		for (let i = 0; i < cluster.length; i++) {
			let point = locationPoints[cluster[i]];
			points.push({ lat: point.lat, lng: point.lng });
		}

		return points;
	}

	bestClusterAt(clusters, locationPoints, startHour, endHour) {
		let bestAt = { c: 0, num: 0 };

		for (let i = 0; i < clusters.length; i++) {
			let cAtDay = this.betweenTwoDates(clusters[i], locationPoints, startHour, endHour);
			if (cAtDay > bestAt.num) {
				bestAt = { c: i, num: cAtDay };
			}
		}

		return bestAt;
	}

	betweenTwoDates(cluster, locationPoints, startHour, endHour) {
		let numOfPoints = 0;
		for (let i = 0; i < cluster.length; i++) {
			const date = locationPoints[cluster[i]].date;
			const hour = date.getHours();
			if (hour > startHour && hour < endHour) {
				numOfPoints++;
			}
		}

		return numOfPoints;
	}

	labelUserLocation(lat: Number, lng: Number, home, work) {
		try {
			if (Array.isArray(home)) {
				if (this.isPartOfCluster(this.transformArray(home), [ lat, lng ])) {
					return 'HOME';
				} else if (this.isPartOfCluster(this.transformArray(work), [ lat, lng ])) {
					return 'WORK';
				} else {
					return 'OTHER';
				}
			} else {
				let distanceToHome = this.getDistance(lat, lng, home.lat, home.lng);
				let distanceToWork = this.getDistance(lat, lng, work.lat, work.lng);

				if (distanceToHome <= this.GLOBAL_RADIUS * 0.001) {
					return 'HOME';
				} else if (distanceToWork <= this.GLOBAL_RADIUS * 0.001) {
					return 'WORK';
				} else {
					return 'OTHER';
				}
			}
		} catch (e) {
			console.log(e);
			return 'OTHER';
		}
	}

	transformArray(points) {
		let result = [];
		for (let i = 0; i < points.length; i++) {
			result.push([ points[i].lat, points[i].lng ]);
		}

		return result;
	}

	isPartOfCluster(points, newPoint) {
		let dbscan = new clustering.DBSCAN();
		points.push(newPoint);
		dbscan.run(points, this.DBSCAN_RADIUS, this.DBSCAN_MINPOINTS);

		return dbscan.noise == null || dbscan.noise.length === 0;
	}

	parseFileData(data) {
		let lines = data.split(/\r?\n/);
		let dbScanPoints = [];
		let locationPoints = [];

		for (let i = 0; i < lines.length; i++) {
			let locationData = lines[i].split(';');
			dbScanPoints.push([ locationData[0], locationData[1] ]); // [Lat, Lng]
			locationPoints.push({ lat: locationData[0], lng: locationData[1], date: new Date(locationData[2]) });
		}

		return { dbScanPoints, locationPoints };
	}

	getLocationFromStorage() {
		Storage.get({ key: 'userLocation' }).then((data) => {
			const storedData = JSON.parse(data.value);
			if (storedData != null) {
				this.setContextUserLocationStorage(storedData.home, storedData.work);
			}

			this.initializeLocation();
		});
	}

	returnCurrentDate() {
		const date = new Date();
		const h = date.getHours();
		const m = date.getMinutes();

		let status: 'MORNING' | 'DAY' | 'EVENING' | 'NIGHT' = 'DAY';

		if (h > 7 && h <= 12) {
			status = 'MORNING';
		} else if (h > 12 && h <= 18) {
			status = 'DAY';
		} else if (h > 18 && h <= 23) {
			status = 'EVENING';
		} else {
			status = 'NIGHT';
		}

		return { status, date };
	}

	initializeLocation() {
		this.requestUserLocation();
		this.periodicGetUserLocation = setInterval(() => {
			this.requestUserLocation();
		}, this.USER_LOCATION_GET_PERIOD_IN_MILIS);
	}

	initializeBrightness() {
		this.requestBrightness();
		this.periodicGetBrightnessInfo = setInterval(() => {
			this.requestBrightness();
		}, this.BRIGHTNESS_GET_PERIOD_IN_MILIS);
	}

	initializeBatteryStatus() {
		this.batterySubscription = this.batteryStatus.onChange().subscribe((data) => {
			if (data.hasOwnProperty('level') && data.hasOwnProperty('isPlugged')) {
				let batteryStatus = 'MEDIUM';
				data.level /= 100;
				if (data.isPlugged || data.level > 0.6) {
					batteryStatus = 'HIGH';
				} else if (data.level <= 0.6 && data.level > 15) {
					batteryStatus = 'MEDIUM';
				} else {
					batteryStatus = 'LOW';
				}
				this.setContextBatteryStatus(batteryStatus, data.level, data.isPlugged);
			} else {
				this.setContextBatteryStatus('MEDIUM', 0.5, false);
			}
		});
	}

	initializeActivityPlugin() {
		document.addEventListener('updateUserActivities', this.activitiesListener);
		document.addEventListener('updateUserActivitiesError', this.activitiesListenerError);
		SmartAdsPlugin.enableActivityUpdates({ duration: 5000 });
	}

	initializeNetwork() {
		Network.getStatus().then((currentStatus) => {
			this.setContextNetworkStatus(currentStatus.connected, currentStatus.connectionType);
			this.networkListener = Network.addListener('networkStatusChange', (status) => {
				this.setContextNetworkStatus(status.connected, status.connectionType);
			});
		});
	}

	requestUserLocation() {
		console.log("UPDATING LOCATION");
		this.forceUpdateLocation();
		/*Geolocation.getCurrentPosition({
			enableHighAccuracy: true,
			timeout: 30000,
			maximumAge: 30000
		}).then((location) => {
			this.setContextUserLocation({ lat: location.coords.latitude, lng: location.coords.longitude });
		});*/
	}

	requestBrightness() {
		SmartAdsPlugin.getBrightness().then((data) => {
			if (data.hasOwnProperty('brightness')) {
				let brightnessStatus = 'LOW';
				if (data.brightness > 128.0) {
					brightnessStatus = 'HIGH';
				}

				this.setContextBrightness(data.brightness, brightnessStatus);
			}
		});
	}

	get context() {
		return this._context.asObservable().pipe(
			map((userContext) => {
				const currentDate = this.returnCurrentDate();
				userContext.time.date = currentDate.date;
				userContext.time.status = currentDate.status;
				return userContext;
			})
		);
	}

	setContext(changedContext: ContextModel) {
		this._context.next(changedContext);
	}

	setContextUserLocation(location) {
		this.context.pipe(take(1)).subscribe((currentContext) => {
			currentContext.userLocation.currentLocation.lat = location.lat;
			currentContext.userLocation.currentLocation.lng = location.lng;

			currentContext.userLocation.currentLocation.status = this.labelUserLocation(
				location.lat,
				location.lng,
				currentContext.userLocation.home,
				currentContext.userLocation.work
			);

			this._context.next(currentContext);
		});
	}

	setContextUserLocationStorage(home, work) {
		this.context.pipe(take(1)).subscribe((currentContext) => {
			currentContext.userLocation.home = home;
			currentContext.userLocation.work = work;

			const lat = currentContext.userLocation.currentLocation.lat;
			const lng = currentContext.userLocation.currentLocation.lng;

			currentContext.userLocation.currentLocation.status = this.labelUserLocation(
				lat,
				lng,
				currentContext.userLocation.home,
				currentContext.userLocation.work
			);

			this._context.next(currentContext);
		});
	}

	setContextBrightness(value: number, status) {
		this.context.pipe(take(1)).subscribe((currentContext) => {
			currentContext.brightness = { value, status };
			this._context.next(currentContext);
		});
	}

	setContextActivity(name: string, confidence: number) {
		this.context.pipe(take(1)).subscribe((currentContext) => {
			currentContext.activity = { name, confidence };
			this._context.next(currentContext);
		});
	}

	setContextNetworkStatus(connected: boolean, connectionType) {
		this.context.pipe(take(1)).subscribe((currentContext) => {
			currentContext.connection = { connected, connectionType };
			this._context.next(currentContext);
		});
	}

	setContextBatteryStatus(status, level, isPlugged) {
		this.context.pipe(take(1)).subscribe((currentContext) => {
			currentContext.battery = { status, level, isPlugged };
			this._context.next(currentContext);
		});
	}

	activitiesListener = (data) => {
		const mostLikelyActivity = this.getMaximumConfidence(data.activities);
		switch (mostLikelyActivity.name) {
			case 'RUNNING':
				mostLikelyActivity.name = 'ON_FOOT';
				break;
			case 'WALKING':
				mostLikelyActivity.name = 'ON_FOOT';
				break;
			case 'ON_FOOT':
				break;
			case 'IN_VEHICLE':
				break;
			default:
				mostLikelyActivity.name = 'STILL';
				break;
		}

		this.setContextActivity(mostLikelyActivity.name, mostLikelyActivity.confidence);
	};

	getMaximumConfidence(activities) {
		let maxConfidenceObj = { name: 'STILL', confidence: 100 };
		let maxConfidence = 0;

		for (let key in activities) {
			if (maxConfidence < activities[key].confidence) {
				maxConfidence = activities[key].confidence;
				maxConfidenceObj = { ...activities[key] };
			}
		}

		return maxConfidenceObj;
	}

	activitiesListenerError = (err) => {
		console.log(err);
	};

	destroyActivitiesListener() {
		if (this.platform.is('mobile')) {
			document.removeEventListener('updateUserActivities', this.activitiesListener);
			document.removeEventListener('updateUserActivitiesError', this.activitiesListenerError);

			if(this.networkListener) {
				this.networkListener.remove();
			}

			SmartAdsPlugin.disableActivityUpdates();

			if (this.batterySubscription) {
				this.batterySubscription.unsubscribe();
			}

			if (this.periodicGetBrightnessInfo) {
				clearInterval(this.periodicGetBrightnessInfo);
			}

			if (this.periodicGetUserLocation) {
				clearInterval(this.periodicGetUserLocation);
			}
		}
	}

	rad(x) {
		return x * Math.PI / 180;
	}

	degreesToRadians(degrees) {
		return degrees * Math.PI / 180;
	}

	/* Calculate the air distance between two places */
	getDistance(lat1, lon1, lat2, lon2) {
		const earthRadiusKm = 6371;

		const dLat = this.degreesToRadians(lat2 - lat1);
		const dLon = this.degreesToRadians(lon2 - lon1);

		lat1 = this.degreesToRadians(lat1);
		lat2 = this.degreesToRadians(lat2);

		const a =
			Math.sin(dLat / 2) * Math.sin(dLat / 2) +
			Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
		const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		return earthRadiusKm * c;
	}
}
