import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { take, switchMap, tap } from 'rxjs/operators';
import { AuthService } from 'src/app/auth/auth.service';
import { UserModel } from 'src/app/auth/user.model';
import { TaskModel } from 'src/app/smartads/task/task.model';

@Injectable()
export class HttpService {
	private API_URL = 'http://163.172.169.249:9080';
	private CORS_PROXY = 'https://cors-anywhere.herokuapp.com/';

	constructor(private http: HttpClient, private authService: AuthService) {}

	getRandomImage() {
		return this.authService.user.pipe(
			take(1),
			switchMap((user: UserModel) => {
				const headers = new HttpHeaders({
					'X-Access-Token': user.token
				});

				return this.http
					.get<any>(`${this.API_URL}/file/image`, {
						headers
					})
					.pipe(
						tap((serverRes) => {
							console.log(serverRes);
							serverRes.url = `${this.API_URL}/${serverRes.url}`;
						})
					);
			})
		);
	}

	getRandomVideo(videoOpt: string) {
		return this.authService.user.pipe(
			take(1),
			switchMap((user: UserModel) => {
				const headers = new HttpHeaders({
					'X-Access-Token': user.token
				});

				return this.http
					.get<any>(`${this.API_URL}/file/video/${videoOpt}`, {
						headers
					})
					.pipe(
						tap((serverRes) => {
							console.log(serverRes);
							serverRes.url = `${this.API_URL}/${serverRes.url}`;
						})
					);
			})
		);
	}

	getAllBoni() {
		return this.authService.user.pipe(
			take(1),
			switchMap((user: UserModel) => {
				const headers = new HttpHeaders({
					'X-Access-Token': user.token
				});

				return this.http.get<any>(`${this.API_URL}/boni`, {
					headers
				});
			})
		);
	}

	sendSemanticLocation(data) {
		console.log('sending to server');
		return this.authService.user.pipe(
			take(1),
			switchMap((user: UserModel) => {
				const headers = new HttpHeaders({
					'X-Access-Token': user.token
				});

				return this.http.post<any>(
					`${this.API_URL}/data/location`,
					{ data },
					{
						headers
					}
				);
			})
		);
	}

	sendFeedback(feedback) {
		return this.authService.user.pipe(
			take(1),
			switchMap((user: UserModel) => {
				const headers = new HttpHeaders({
					'X-Access-Token': user.token
				});

				return this.http.post<any>(`${this.API_URL}/data/feedback`, feedback, {
					headers
				});
			})
		);
	}

	sendData(data) {
		return this.authService.user.pipe(
			take(1),
			switchMap((user: UserModel) => {
				const headers = new HttpHeaders({
					'X-Access-Token': user.token
				});

				return this.http.post<any>(`${this.API_URL}/data`, data, {
					headers
				});
			})
		);
	}

	getBusesToCenter() {
		return this.authService.user.pipe(
			take(1),
			switchMap((user: UserModel) => {
				const headers = new HttpHeaders({
					'X-Access-Token': user.token
				});

				return this.http.get<any>(`${this.API_URL}/bus/toCenter`, {
					headers
				});
			})
		);
	}

	getBusesFromCenter() {
		return this.authService.user.pipe(
			take(1),
			switchMap((user: UserModel) => {
				const headers = new HttpHeaders({
					'X-Access-Token': user.token
				});

				return this.http.get<any>(`${this.API_URL}/bus/fromCenter`, {
					headers
				});
			})
		);
	}

	getSpecificStationInformation(stationId: string) {
		return this.authService.user.pipe(
			take(1),
			switchMap((user: UserModel) => {
				const headers = new HttpHeaders({
					'X-Access-Token': user.token
				});

				return this.http.get<any>(`${this.API_URL}/bus/${stationId}`, {
					headers
				});
			})
		);
	}

	getMyTasks() {
		return this.authService.user.pipe(
			take(1),
			switchMap((user: UserModel) => {
				const headers = new HttpHeaders({
					'X-Access-Token': user.token
				});

				return this.http.get<any>(`${this.API_URL}/task`, {
					headers
				});
			})
		);
	}

	getSpecificTask(taskId: string) {
		return this.authService.user.pipe(
			take(1),
			switchMap((user: UserModel) => {
				const headers = new HttpHeaders({
					'X-Access-Token': user.token
				});

				return this.http.get<any>(`${this.API_URL}/task/${taskId}`, {
					headers
				});
			})
		);
	}

	createTask(taskModel: TaskModel) {
		return this.authService.user.pipe(
			take(1),
			switchMap((user: UserModel) => {
				const headers = new HttpHeaders({
					'X-Access-Token': user.token
				});

				return this.http.post<any>(`${this.API_URL}/task/create`, taskModel, {
					headers
				});
			})
		);
	}

	updateTask(taskId: string, taskModel: TaskModel) {
		return this.authService.user.pipe(
			take(1),
			switchMap((user: UserModel) => {
				const headers = new HttpHeaders({
					'X-Access-Token': user.token
				});

				return this.http.put<any>(`${this.API_URL}/task/${taskId}`, taskModel, {
					headers
				});
			})
		);
	}

	deleteTask(taskId: string) {
		return this.authService.user.pipe(
			take(1),
			switchMap((user: UserModel) => {
				const headers = new HttpHeaders({
					'X-Access-Token': user.token
				});

				return this.http.delete<any>(`${this.API_URL}/task/${taskId}`, {
					headers
				});
			})
		);
	}

	getTrainStations(direction: string) {
		return this.authService.user.pipe(
			take(1),
			switchMap((user: UserModel) => {
				const headers = new HttpHeaders({
					'X-Access-Token': user.token
				});

				return this.http.get<any>(`${this.API_URL}/train/${direction}`, {
					headers
				});
			})
		);
	}

	getMyTimetable() {
		return this.authService.user.pipe(
			take(1),
			switchMap((user: UserModel) => {
				const headers = new HttpHeaders({
					'X-Access-Token': user.token
				});

				return this.http.get<any>(`${this.API_URL}/timetable`, {
					headers
				});
			})
		);
	}

	addNewCourse(data: any) {
		return this.authService.user.pipe(
			take(1),
			switchMap((user: UserModel) => {
				const headers = new HttpHeaders({
					'X-Access-Token': user.token
				});

				return this.http.post<any>(`${this.API_URL}/timetable/create`, data, {
					headers
				});
			})
		);
	}

	removeCourse(index: number, day: 'Pon' | 'Tor' | 'Sre' | 'Cet' | 'Pet') {
		return this.authService.user.pipe(
			take(1),
			switchMap((user: UserModel) => {
				const headers = new HttpHeaders({
					'X-Access-Token': user.token
				});

				return this.http.put<any>(
					`${this.API_URL}/timetable/remove`,
					{ index, day },
					{
						headers
					}
				);
			})
		);
	}

	updateCourse(data: any) {
		return this.authService.user.pipe(
			take(1),
			switchMap((user: UserModel) => {
				const headers = new HttpHeaders({
					'X-Access-Token': user.token
				});

				return this.http.put<any>(`${this.API_URL}/timetable/update`, data, {
					headers
				});
			})
		);
	}

	getNewsFeed(url) {
		return this.http.get(`${this.CORS_PROXY}${url}`, {
			headers: new HttpHeaders({ Accept: 'application/rss+xml' }),
			responseType: 'text'
		});
	}
}
