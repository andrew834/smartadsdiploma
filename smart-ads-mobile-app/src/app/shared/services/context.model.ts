export class ContextModel {
	constructor(
		public activity: {
            name: string;
            confidence: number; // 0 - 100
        },
        public connection: {
            connected: boolean;
            connectionType: 'wifi' | 'cellular' | 'none' | 'unknown';
        },
        public battery: {
            status: 'LOW' | 'MEDIUM' | 'HIGH';
            level: number; // 0 - 1
            isPlugged: boolean;
        },
        public brightness: {
            status: 'LOW' | 'HIGH';
            value: number; // 0 - 255
        },
        public time : {
            status: 'NIGHT' | 'MORNING' | 'DAY' | 'EVENING', // 23:00 - 7:00 - 12:00 - 18:00 - ...
            date: Date
        },
        public userLocation?: {
            home: {
                lat: Number,
                lng: Number
            } | [{ lat: Number, lng: Number }],
            work: {
                lat: Number,
                lng: Number
            } | [{ lat: Number, lng: Number }],
            currentLocation: {
                lat: Number,
                lng: Number,
                status: 'HOME' | 'WORK' | 'OTHER'
            }
        }
	) {}
}

/**
 * Activity:
 *  name has the following possibilities: UNKNOWN, ON_FOOT(RUNNING, WALKING), ON_BICYCLE, IN_VEHICLE, STILL, TILTING
 *  some of them might be redundant (TILTING, ON_BICYCLE, ...) 
 * 
 * Connection:
 *  should also consider strength level of the connection?
 * 
 * Brightness:
 *  considers the screen brightness of the phone, not the environment (modern phones have an inbuilt feature that changes
 *  screen brightness depending on the environment)
 */