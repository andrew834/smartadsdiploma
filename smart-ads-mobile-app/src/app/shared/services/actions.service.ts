import { Injectable } from '@angular/core';
import { Plugins } from '@capacitor/core';
import { NavController, ModalController } from '@ionic/angular';
import { AdsComponent } from 'src/app/smartads/ads/ads.component';

const { Storage } = Plugins;

@Injectable()
export class ActionsService {
    currentActions;
    maxActionsBeforeAd = 5;

    constructor(private navCtrl: NavController, private modalCtrl: ModalController) {
        Storage.get({key: "smart-ads-actions"}).then(storageData => {
            const data = JSON.parse(storageData.value);
            this.currentActions = data == null ? 0 : data.stored;
        });
    }

    actionFiredShowAd() {
        this.currentActions++;
        if(this.currentActions == this.maxActionsBeforeAd){
            this.currentActions = 0;
            Storage.set({key: "smart-ads-actions", value: JSON.stringify({stored: this.currentActions})});
            setTimeout(() => {
                this.modalCtrl.create({
                    component: AdsComponent,
                    backdropDismiss: false,
                    animated: true
                }).then(modalEl => {
                    modalEl.present();
                });
            }, 250);
        } else {
            Storage.set({key: "smart-ads-actions", value: JSON.stringify({stored: this.currentActions})});
        }
    }
}
