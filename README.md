# SmartAds Diploma

Poganjanje strežnika z vsebniško tehnologijo Docker (Windows - dostop do strežnika preko IP naslova **192.168.99.100**):
```sh
	./run.sh <arg>
	<arg>: "clean" - odstrani vse volume in počisti vse nastavitve Docker shell-a. Za tem se sprožijo vsi ukazi.
    	   "create-network" - ustvari omrežje, preko katerega bodo storitve komunicirale
		   "create-volume" - ustvari volume, kamor se bodo shranjevali podatki iz baze
		   "fresh" - kombinacija ukazov "create-network" in "create-volume"
```

Dostop do Traefik nadzorne plošče (samo v *insecure* načinu): **http://192.168.99.100:8080** ali **http://PUBLIC_IP:8080**

Dostop do strežnika: **http://192.168.99.100:9080** ali **http://PUBLIC_IP:9080**

Morebitne težave:

```sh
	npm install --save @capacitor/core @capacitor/cli
	npm install --global --production windows-build-tools
	npm install -g cordova-check-plugins
	cordova-check-plugins --update=auto
```

Testiranje na platofrmi Android:

```sh
	ng build
	npx cap sync android
	npx cap copy android
	npx cap open android
```
