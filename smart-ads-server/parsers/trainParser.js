/**
 * 1. Go to: https://www.slo-zeleznice.si/sl
 * 2. Paste and call the following function: getElementsJson()
 * 3. Copy both outputs into two seperate *.json files
 *
 * @result Fetches all stations that currently exist.
 */

function getElementsJson() {
    extractData(document.getElementById('vstopnaPostaja').childNodes);
    console.log('---------------------------------------------------');
    extractData(document.getElementById('izstopnaPostaja').childNodes);
}

function extractData(stations) {
    let hashmap = Object.create(null);

    for (let i = 0; i < stations.length; i++) {
        let newObj = Object.create(null);

        if (stations[i].nodeName.toLowerCase() !== 'option') {
            continue;
        }

        const id = stations[i].value;
        const name = stations[i].innerText;
        hashmap[id] = name;
    }

    console.log(JSON.stringify(hashmap));
}