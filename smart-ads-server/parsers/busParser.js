/**
 * 1. Go to: https://www.lpp.si/napovedi-prihodov-avtobusov
 * 2. Select the 'V center' button
 * 3. Paste and call the following function: getElementsJson()
 * 4. Copy the output into a *.json file
 * 5. Select the 'Iz centra' button
 * 6. Call the following function: getElementsJson()
 * 7. Copy the output into a *.json file
 *
 * @result Fetches all stations that currently exist.
 */

function getElementsJson() {
    const stations = document.querySelectorAll('#station-select-list option');
    let hashmap = Object.create(null);

    for (let i = 0; i < stations.length; i++) {
        let newObj = Object.create(null);
        const id = stations[i].getAttribute('data-id');
        const name = stations[i].value.replace(`(${id})`, '');
        hashmap[id] = name;
    }

    console.log(JSON.stringify(hashmap));
}