/**
 * 1. Go to: https://www.studentska-prehrana.si/sl/restaurant
 * 2. Paste and call the following function: getElementsJson()
 * 3. Copy the output into a *.json file
 *
 * @result Fetches all restaurants for students.
 *
 * Optional
 *
 * To fill the parsed data with extra information (lat, lng, rating) call the following function: getExtraInfo(), located in the api/controllers/boni.js
 *
 */

function getElementsJson() {
    const titles = $('h2 a');
    const addresses = $('small i');
    const cene = $('.color-light-grey');

    let array = [];

    for (let i = 0; i < addresses.length; i++) {
        let newObj = Object.create(null);
        newObj.address = addresses[i].innerHTML.trim();
        newObj.href = titles[i].href;
        newObj.title = titles[i].innerHTML.trim();
        newObj.cenaDoplacila = cene[i * 2].innerHTML.trim();
        newObj.cenaObroka = cene[i * 2 + 1].innerHTML.trim();
        array.push(newObj);
    }

    console.log(JSON.stringify(array));
}