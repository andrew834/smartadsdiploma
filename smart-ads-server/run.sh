#!/bin/bash
clear
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)

if [ "$1" == "clean" ]; then
  echo "Attempting to clean docker volumes..."
  docker volume prune
  echo "Force clear images and volumes..."
  docker rmi -f $(docker images --filter dangling=true -qa)
  docker volume rm $(docker volume ls --filter dangling=true -q)
  docker rmi -f $(docker images -qa)
  echo "Attempting to clean the system..."
  docker system prune -a
fi

if [ "$1" == "fresh" ] || [ "$1" == "create-volume" ] || [ "$1" == "clean" ]; then
  echo "Creating volume: mongodbdata, size=5GB"
  docker volume create --opt o=size=5GB mongodbdata
fi

if [ "$1" == "fresh" ] || [ "$1" == "create-network" ] || [ "$1" == "clean" ]; then
  echo "Creating custom network: smart-ads-network"
  docker network create smart-ads-network
fi
  
docker build -t smart-ads-server .
docker-compose up -d
docker-compose scale smart-ads-server=4