const express = require('express');
const router = express.Router();
const ctrlBus = require('../controllers/bus');
const VerifyToken = require('../validators/verifyToken');

// Route prefix: /bus

router.get('/toCenter', VerifyToken.verifyToken, ctrlBus.getAllStationsToCenter);

router.get('/fromCenter', VerifyToken.verifyToken, ctrlBus.getAllStationsFromCenter);

router.get('/:stationId', VerifyToken.verifyToken, ctrlBus.getSpecificStationInformation);

module.exports = router;