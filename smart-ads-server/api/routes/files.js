const express = require('express');
const router = express.Router();
const ctrlFiles = require('../controllers/files');
const VerifyToken = require('../validators/verifyToken');

// Route prefix: /file

router.get('/image', VerifyToken.verifyToken, ctrlFiles.sendRandomImage);
router.get('/video/short', VerifyToken.verifyToken, ctrlFiles.sendRandomVideoShort);
router.get('/video/long', VerifyToken.verifyToken, ctrlFiles.sendRandomVideoLong);

module.exports = router;