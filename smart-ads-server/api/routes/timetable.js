const express = require('express');
const router = express.Router();
const ctrlTimetable = require('../controllers/timetable');
const VerifyToken = require('../validators/verifyToken');
const VerifyTimetable = require('../validators/verifyTimetable');

// Route prefix: /timetable

router.get('/', VerifyToken.verifyToken, ctrlTimetable.getMyTimetable);

router.post('/create', VerifyToken.verifyToken, VerifyTimetable.verifyTimetable, ctrlTimetable.addNewCourse);

router.put('/remove', VerifyToken.verifyToken, VerifyTimetable.verifyRemove, ctrlTimetable.deleteSpecificCourse);

router.put(
    '/update',
    VerifyToken.verifyToken,
    VerifyTimetable.verifyTimetableUpdate,
    ctrlTimetable.updateSpecificCourse
);

module.exports = router;