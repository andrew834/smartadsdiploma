const express = require('express');
const router = express.Router();
const ctrlUsers = require('../controllers/users');
const VerifyUser = require('../validators/verifyUser');
const VerifyToken = require('../validators/verifyToken');

// Route prefix: /user

router.post('/register', VerifyUser.verifyRegistration, ctrlUsers.register);
router.post('/login', VerifyUser.verifyLogin, ctrlUsers.login);
router.get('/refresh', VerifyToken.verifyToken, ctrlUsers.refresh);
router.post('/all', VerifyToken.admin, ctrlUsers.all);

module.exports = router;