const express = require('express');
const router = express.Router();
const ctrlTasks = require('../controllers/tasks');
const VerifyToken = require('../validators/verifyToken');
const VerifyTask = require('../validators/verifyTask');

// Route prefix: /task

router.get('/', VerifyToken.verifyToken, ctrlTasks.getMyTasks);

router.post('/create', VerifyToken.verifyToken, VerifyTask.verifyTask, ctrlTasks.createTask);

router.get('/:taskId', VerifyToken.verifyToken, ctrlTasks.getSpecificTask);

router.delete('/:taskId', VerifyToken.verifyToken, ctrlTasks.deleteSpecificTask);

router.put('/:taskId', VerifyToken.verifyToken, VerifyTask.verifyTask, ctrlTasks.updateSpecificTask);

module.exports = router;