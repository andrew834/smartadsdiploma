const express = require('express');
const router = express.Router();
const ctrlTrain = require('../controllers/train');
const VerifyToken = require('../validators/verifyToken');

// Route prefix: /train

router.get('/in', VerifyToken.verifyToken, ctrlTrain.getAllStationsIn);
router.get('/out', VerifyToken.verifyToken, ctrlTrain.getAllStationsOut);

module.exports = router;