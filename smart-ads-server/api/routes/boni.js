const express = require('express');
const router = express.Router();
const ctrlBoni = require('../controllers/boni');
const VerifyToken = require('../validators/verifyToken');

// Route prefix: /boni

router.get('/', VerifyToken.verifyToken, ctrlBoni.getAll);

module.exports = router;