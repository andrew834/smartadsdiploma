const express = require('express');
const router = express.Router();
const ctrlData = require('../controllers/data');
const VerifyToken = require('../validators/verifyToken');
const VerifyData = require('../validators/verifyData');

// Route prefix: /data

router.post('/', VerifyToken.verifyToken, VerifyData.verifyData, ctrlData.addNewData);

router.post('/all', VerifyToken.admin, ctrlData.all);

router.post('/location', VerifyToken.verifyToken, VerifyData.verifyLocationUpdate, ctrlData.updateLocation);

router.post('/feedback', VerifyToken.verifyToken, VerifyData.verifyFeedback, ctrlData.addFeedback);

module.exports = router;