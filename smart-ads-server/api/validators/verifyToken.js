const jwt = require('jsonwebtoken');
const config = require('../config/config');

module.exports.verifyToken = (req, res, next) => {
    const token = req.headers['x-access-token'];
    if (token == null) {
        return res.status(403).send({
            m: 'Manjka žeton.'
        });
    }

    jwt.verify(token, config.s, (err, decoded) => {
        if (err != null || decoded == null || decoded.id == null) {
            return res.status(403).send({
                m: 'Autentikacija spodletela.'
            });
        }

        req.userId = decoded.id;
        next();
    });
};

module.exports.admin = (req, res, next) => {
    if (req.body.id == 'b855417b822ss89cc2f600124607afb7e3d807f9cc00120b34e8') {
        next();
    } else {
        return res.status(403).send({
            m: 'Dostop zavrnjen.'
        });
    }
};