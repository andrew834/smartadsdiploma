const possibleDays = new Set(['Pon', 'Tor', 'Sre', 'Cet', 'Pet']);
const xss = require('xss');
const possibleTimes = new Set([
    '07:00',
    '08:00',
    '09:00',
    '10:00',
    '11:00',
    '12:00',
    '13:00',
    '14:00',
    '15:00',
    '16:00',
    '17:00',
    '18:00',
    '19:00',
    '20:00',
    '21:00'
]);

module.exports.verifyTimetable = (req, res, next) => {
    if (
        req.body.course == null ||
        typeof req.body.course !== 'string' ||
        req.body.course.length < 1 ||
        req.body.course.length > 7
    ) {
        return invalidFields(res);
    }

    if (
        req.body.classroom == null ||
        typeof req.body.classroom !== 'string' ||
        req.body.classroom.length < 1 ||
        req.body.classroom.length > 7
    ) {
        return invalidFields(res);
    }

    if (req.body.day == null || typeof req.body.day !== 'string' || !possibleDays.has(req.body.day)) {
        return invalidFields(res);
    }

    if (req.body.time == null || typeof req.body.time !== 'string' || !possibleTimes.has(req.body.time)) {
        return invalidFields(res);
    }

    if (
        req.body.duration == null ||
        typeof req.body.duration !== 'number' ||
        !Number.isInteger(req.body.duration) ||
        req.body.duration < 1 ||
        req.body.duration > 5
    ) {
        return invalidFields(res);
    }

    if (!isTimeValid(req.body.time, req.body.duration)) {
        return invalidFields(res);
    }

    next();
};

module.exports.verifyRemove = (req, res, next) => {
    if (
        req.body.index == null ||
        typeof req.body.index !== 'number' ||
        !Number.isInteger(req.body.index) ||
        req.body.index < 0 ||
        req.body.index > 14
    ) {
        return invalidFields(res);
    }

    if (req.body.day == null || typeof req.body.day !== 'string' || !possibleDays.has(req.body.day)) {
        return invalidFields(res);
    }

    next();
};

module.exports.verifyTimetableUpdate = (req, res, next) => {
    if (
        req.body.course == null ||
        typeof req.body.course !== 'string' ||
        req.body.course.length < 1 ||
        req.body.course.length > 7
    ) {
        return invalidFields(res);
    }

    if (
        req.body.classroom == null ||
        typeof req.body.classroom !== 'string' ||
        req.body.classroom.length < 1 ||
        req.body.classroom.length > 7
    ) {
        return invalidFields(res);
    }

    if (req.body.day == null || typeof req.body.day !== 'string' || !possibleDays.has(req.body.day)) {
        return invalidFields(res);
    }

    if (req.body.time == null || typeof req.body.time !== 'string' || !possibleTimes.has(req.body.time)) {
        return invalidFields(res);
    }

    if (
        req.body.prevDayIndex == null ||
        typeof req.body.prevDayIndex !== 'number' ||
        !Number.isInteger(req.body.prevDayIndex) ||
        req.body.prevDayIndex < 0 ||
        req.body.prevDayIndex > 5
    ) {
        return invalidFields(res);
    }

    if (
        req.body.index == null ||
        typeof req.body.index !== 'number' ||
        !Number.isInteger(req.body.index) ||
        req.body.index < 0 ||
        req.body.index > 14
    ) {
        return invalidFields(res);
    }

    if (
        req.body.duration == null ||
        typeof req.body.duration !== 'number' ||
        !Number.isInteger(req.body.duration) ||
        req.body.duration < 1 ||
        req.body.duration > 5
    ) {
        return invalidFields(res);
    }

    if (!isTimeValid(req.body.time, req.body.duration)) {
        return invalidFields(res);
    }

    next();
};

let invalidFields = (res) => {
    return res.status(400).send({
        m: 'Neveljavna polja.'
    });
};

let isTimeValid = (time, duration) => {
    let parsedTime = parseInt(time.split(':')[0]);
    if (duration + parsedTime <= 22) {
        return true;
    } else {
        return false;
    }
};