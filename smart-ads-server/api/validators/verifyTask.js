const possibleUrgencies = new Set(['#AFE067', '#FFCF68', '#4C8DFF', '#F25454']);
const xss = require('xss');

module.exports.verifyTask = (req, res, next) => {
    if (
        req.body.title == null ||
        typeof req.body.title !== 'string' ||
        req.body.title.length < 3 ||
        req.body.title.length > 40 ||
        !req.body.title.match(/^[0-9a-žA-Ž ]+$/)
    ) {
        return invalidFields(res);
    }

    if (req.body.description == null || typeof req.body.description !== 'string' || req.body.title.length > 2000) {
        return invalidFields(res);
    }

    if (req.body.urgency == null || typeof req.body.urgency !== 'string' || !possibleUrgencies.has(req.body.urgency)) {
        return invalidFields(res);
    }

    req.body.description = xss(req.body.description);

    next();
};

let invalidFields = (res) => {
    return res.status(400).send({
        m: 'Neveljavna polja.'
    });
};