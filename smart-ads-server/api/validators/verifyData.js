const fs = require('fs');

const possibleCategories = new Set(['image', 'vlong', 'vshort']);
const possibleActivities = new Set(['STILL', 'ON_FOOT', 'IN_VEHICLE']);
const possibleLocations = new Set(['UNKNOWN', 'OTHER', 'WORK', 'HOME']);
const momentJS = require('moment');

const feedbackPath = __dirname + '/../controllers/feedback.json';
const adsPath = __dirname + '/../controllers/ads.json';

const feedback = JSON.parse(fs.readFileSync(feedbackPath));
const ads = JSON.parse(fs.readFileSync(adsPath));

const xss = require('xss');

module.exports.verifyData = (req, res, next) => {
    console.log(req.body);

    if (req.body.name == null || typeof req.body.name !== 'string') {
        console.log('n');
        return invalidFields(res);
    }

    req.body.name = xss(req.body.name);

    if (
        req.body.category == null ||
        typeof req.body.category !== 'string' ||
        !possibleCategories.has(req.body.category)
    ) {
        return invalidFields(res);
    }

    if (
        req.body.duration == null ||
        typeof req.body.duration !== 'number' ||
        req.body.duration < 0 ||
        req.body.duration > 1000
    ) {
        return invalidFields(res);
    }

    if (
        req.body.activity == null ||
        typeof req.body.activity !== 'object' ||
        req.body.activity.name == null ||
        typeof req.body.activity.name !== 'string' ||
        !possibleActivities.has(req.body.activity.name) ||
        req.body.activity.confidence == null ||
        typeof req.body.activity.confidence !== 'number' ||
        req.body.activity.confidence < 0 ||
        req.body.activity.confidence > 100
    ) {
        return invalidFields(res);
    }

    if (req.body.connection == null || typeof req.body.connection !== 'string' || req.body.connection.length >= 10) {
        return invalidFields(res);
    }

    if (
        req.body.battery == null ||
        typeof req.body.battery !== 'object' ||
        req.body.battery.level == null ||
        typeof req.body.battery.level !== 'number' ||
        req.body.battery.level < 0 ||
        req.body.battery.level > 1 ||
        req.body.battery.isPlugged == null ||
        typeof req.body.battery.isPlugged !== 'boolean'
    ) {
        return invalidFields(res);
    }

    if (
        req.body.brightness == null ||
        typeof req.body.brightness !== 'number' ||
        req.body.brightness < 0 ||
        req.body.brightness > 255
    ) {
        return invalidFields(res);
    }

    if (req.body.time == null || typeof req.body.time !== 'string' || !momentJS(req.body.time).isValid()) {
        return invalidFields(res);
    }

    if (
        req.body.location == null ||
        typeof req.body.location !== 'string' ||
        !possibleLocations.has(req.body.location)
    ) {
        return invalidFields(res);
    }

    next();
};

module.exports.verifyFeedback = (req, res, next) => {
    console.log(req.body);

    if (req.body.a1 == null || typeof req.body.a1 !== 'string' || feedback.products.indexOf(req.body.a1) == -1) {
        console.log('a1');
        return invalidFields(res);
    }

    if (req.body.a2 == null || typeof req.body.a2 !== 'string' || feedback.firms.indexOf(req.body.a2) == -1) {
        console.log('a2');
        return invalidFields(res);
    }

    try {
        req.body.a3 = parseInt(req.body.a3);
    } catch (e) {
        console.log(e);
        return invalidFields(res);
    }

    if (
        req.body.a3 == null ||
        typeof req.body.a3 !== 'number' ||
        !Number.isInteger(req.body.a3) ||
        req.body.a3 < -2 ||
        req.body.a3 > 2
    ) {
        console.log('a3');
        return invalidFields(res);
    }

    if (req.body.id == null || typeof req.body.id !== 'string') {
        return invalidFields(res);
    }

    next();
};

module.exports.verifyLocationUpdate = (req, res, next) => {
    console.log('SENDING LOCATIONSSSSSSSSS');
    console.log(req.body);
    if (req.body.data == null || !Array.isArray(req.body.data)) {
        return invalidFields(res);
    }

    for (let i = 0; i < req.body.data.length; i++) {
        if (req.body.data[i] == null || typeof req.body.data[i] !== 'object') {
            return invalidFields(res);
        }

        if (req.body.data[i].id == null || typeof req.body.data[i].id !== 'string') {
            return invalidFields(res);
        }

        if (
            req.body.data[i].location == null ||
            typeof req.body.data[i].location !== 'string' ||
            !possibleLocations.has(req.body.data[i].location)
        ) {
            return invalidFields(res);
        }
    }

    next();
};

let invalidFields = (res) => {
    return res.status(400).send({
        m: 'Neveljavna polja.'
    });
};