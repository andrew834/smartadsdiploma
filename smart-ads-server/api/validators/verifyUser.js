const momentJS = require('moment');
const possibleGenders = new Set(['m', 'f', 'o']);

module.exports.verifyRegistration = (req, res, next) => {
    if (
        req.body.u == null ||
        typeof req.body.u !== 'string' ||
        req.body.u.length < 3 ||
        req.body.u.length > 20 ||
        !req.body.u.match(/^[0-9a-žA-Ž]+$/)
    ) {
        return invalidFields(res);
    }

    if (req.body.p == null || typeof req.body.p !== 'string' || req.body.p.length < 6 || req.body.p.length > 20) {
        return invalidFields(res);
    }

    if (req.body.rP !== req.body.p) {
        return invalidFields(res);
    }

    if (req.body.b == null || typeof req.body.b !== 'string' || !momentJS(req.body.b).isValid()) {
        return invalidFields(res);
    }

    if (req.body.g == null || typeof req.body.g !== 'string' || !possibleGenders.has(req.body.g)) {
        return invalidFields(res);
    }

    next();
};

module.exports.verifyLogin = (req, res, next) => {
    if (req.body.u == null || typeof req.body.u !== 'string') {
        return invalidFields(res);
    }

    if (req.body.p == null || typeof req.body.p !== 'string') {
        return invalidFields(res);
    }

    next();
};

let invalidFields = (res) => {
    return res.status(400).send({
        m: 'Neveljavna polja.'
    });
};