const mongoose = require('mongoose');
const User = mongoose.model('User');
const Timetable = mongoose.model('Timetable');

module.exports.getMyTimetable = (req, res) => {
    const userId = req.userId;

    User.findOne({ _id: userId }, { timetable: 1 })
        .populate('timetable')
        .then((userTimetable) => {
            if (userTimetable === null) {
                return res.status(400).send({
                    m: 'Prišlo je do napake, uporabnik ne obstaja.'
                });
            }
            res.status(200).send({
                t: userTimetable.timetable
            });
        })
        .catch((err) => {
            console.log(err);
            res.status(500).send({
                m: 'Prišlo je do napake na strežniku.'
            });
        });
};

module.exports.addNewCourse = (req, res) => {
    const userId = req.userId;
    let index;

    User.findOne({ _id: userId })
        .populate('timetable')
        .then((user) => {
            if (user == null || user.timetable.length === 0) {
                throw `Prišlo je do kritične napake za uporabnika ${userId}. Token veljaven, a uporabnika ni v bazi.`;
            }

            index = findTimetableIndex(req.body.day, user.timetable);

            if (isOverlaping(req.body, user.timetable[index].table)) {
                throw 'Prišlo je do prekrivanj v urniku.';
            } else {
                return Timetable.findOne({ _id: user.timetable[index]._id });
            }
        })
        .then((timetable) => {
            if (timetable == null) {
                throw `Prišlo je do kritične napake za uporabnika ${userId}, manjkajoči urnik.`;
            }
            let startTime = parseInt(req.body.time.split(':')[0]);
            let requestedDuration = req.body.duration;
            let endTime = startTime - 7 + requestedDuration;

            for (let i = startTime - 7; i < endTime; i++) {
                timetable.table[i].course = req.body.course;
                timetable.table[i].duration = req.body.duration;
                timetable.table[i].classroom = req.body.classroom;
            }

            return timetable.save();
        })
        .then((_) => {
            res.status(200).send({ m: 'Success.' });
        })
        .catch((err) => {
            console.log(err);
            res.status(500).send({
                m: 'Prišlo je do napake na strežniku.'
            });
        });
};

module.exports.deleteSpecificCourse = (req, res) => {
    const userId = req.userId;
    let index;

    User.findOne({ _id: userId })
        .populate('timetable')
        .then((user) => {
            if (user == null || user.timetable.length === 0) {
                throw `Prišlo je do kritične napake za uporabnika ${userId}. Token veljaven, a uporabnika ni v bazi.`;
            }

            index = findTimetableIndex(req.body.day, user.timetable);

            return Timetable.findOne({ _id: user.timetable[index]._id });
        })
        .then((timetable) => {
            if (timetable == null) {
                throw `Prišlo je do kritične napake za uporabnika ${userId}, manjkajoči urnik.`;
            }

            let startTime = parseInt(timetable.table[req.body.index].time.split(':')[0]);
            let requestedDuration = timetable.table[req.body.index].duration;
            let endTime = startTime - 7 + requestedDuration;

            for (let i = startTime - 7; i < endTime; i++) {
                timetable.table[i].course = ' ';
                timetable.table[i].duration = 0;
                timetable.table[i].classroom = ' ';
            }

            return timetable.save();
        })
        .then((_) => {
            res.status(200).send({ m: 'Success.' });
        })
        .catch((err) => {
            console.log(err);
            res.status(500).send({
                m: 'Prišlo je do napake na strežniku.'
            });
        });
};

module.exports.updateSpecificCourse = (req, res) => {
    const userId = req.userId;
    let index;
    let user;

    User.findOne({ _id: userId })
        .populate('timetable')
        .then((userObj) => {
            if (userObj == null || userObj.timetable.length === 0) {
                throw `Prišlo je do kritične napake za uporabnika ${userId}. Token veljaven, a uporabnika ni v bazi.`;
            }

            user = userObj;
            index = findTimetableIndex(req.body.day, user.timetable);

            return Timetable.findOne({ _id: user.timetable[req.body.prevDayIndex]._id });
        })
        .then((timetable) => {
            if (timetable == null) {
                throw `Prišlo je do kritične napake za uporabnika ${userId}, manjkajoči urnik 1.`;
            }

            let startTime = parseInt(timetable.table[req.body.index].time.split(':')[0]);
            let requestedDuration = timetable.table[req.body.index].duration;
            let endTime = startTime - 7 + requestedDuration;

            for (let i = startTime - 7; i < endTime; i++) {
                timetable.table[i].course = ' ';
                timetable.table[i].duration = 0;
                timetable.table[i].classroom = ' ';
            }

            return timetable.save();
        })
        .then((_) => {
            return Timetable.findOne({ _id: user.timetable[index]._id });
        })
        .then((timetable) => {
            if (timetable == null) {
                throw `Prišlo je do kritične napake za uporabnika ${userId}, manjkajoči urnik 2.`;
            }

            let startTime = parseInt(timetable.table[req.body.index].time.split(':')[0]);
            let requestedDuration = timetable.table[req.body.index].duration;
            let endTime = startTime - 7 + requestedDuration;

            for (let i = startTime - 7; i < endTime; i++) {
                timetable.table[i].course = ' ';
                timetable.table[i].duration = 0;
                timetable.table[i].classroom = ' ';
            }

            if (isOverlaping(req.body, timetable.table)) {
                throw 'Prišlo je do prekrivanj v urniku.';
            } else {
                let startTime = parseInt(req.body.time.split(':')[0]);
                let requestedDuration = req.body.duration;
                let endTime = startTime - 7 + requestedDuration;

                for (let i = startTime - 7; i < endTime; i++) {
                    timetable.table[i].course = req.body.course;
                    timetable.table[i].duration = req.body.duration;
                    timetable.table[i].classroom = req.body.classroom;
                }

                return timetable.save();
            }
        })
        .then((_) => {
            res.status(200).send({ m: 'Success.' });
        })
        .catch((err) => {
            console.log(err);
            res.status(500).send({
                m: 'Prišlo je do napake na strežniku.'
            });
        });
};

let findTimetableIndex = (day, timetable) => {
    for (let i = 0; timetable.length; i++) {
        if (timetable[i].day === day) {
            return i;
        }
    }
};

let isOverlaping = (data, eventsDayArr) => {
    let startTime = parseInt(data.time.split(':')[0]);
    let requestedDuration = data.duration;

    let endTime = startTime - 7 + requestedDuration;
    for (let i = startTime - 7; i < endTime; i++) {
        if (eventsDayArr[i].duration !== 0) {
            return true;
        }
    }

    return false;
};