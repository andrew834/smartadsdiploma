const mongoose = require('mongoose');
const User = mongoose.model('User');
const Timetable = mongoose.model('Timetable');
const Tableslot = mongoose.model('Tableslot');

module.exports.register = (req, res) => {
    let user = new User();
    user.u = req.body.u;
    user.b = req.body.b;
    user.g = req.body.g;
    user.dataCollection = [];
    user.setPassword(req.body.p);

    user
        .save()
        .then((_) => {
            return createTimetableEntries();
        })
        .then((arrRes) => {
            try {
                for (let i = 0; i < arrRes.length; i++) {
                    user.timetable.push(arrRes[i]._id);
                }
                return user.save();
            } catch (e) {
                console.log(e);
                return res.status(500).send({
                    m: 'Prišlo je do napake na strežniku.'
                });
            }
        })
        .then((_) => {
            res.status(200).send({
                t: user.generateToken()
            });
        })
        .catch((err) => {
            console.log(err);
            res.status(400).send({
                m: 'Uporabniško ime je že zasedeno.'
            });
        });
};

module.exports.login = (req, res) => {
    User.findOne({ u: req.body.u })
        .then((user) => {
            if (user == null || !user.checkPassword(req.body.p)) {
                res.status(400).send({
                    m: 'Napaćno uporabniško ime ali geslo.'
                });
            } else {
                res.status(200).send({
                    t: user.generateToken()
                });
            }
        })
        .catch((err) => {
            console.log(err);
            res.status(500).send({
                m: 'Prišlo je do napake na strežniku.'
            });
        });
};

module.exports.refresh = (req, res) => {
    User.findOne({ _id: req.userId })
        .then((user) => {
            if (user != null) {
                res.status(200).send({
                    t: user.generateToken()
                });
            } else {
                res.status(500).send({
                    m: 'Prišlo je do napake na strežniku.'
                });
            }
        })
        .catch((err) => {
            console.log(err);
            res.status(500).send({
                m: 'Prišlo je do napake na strežniku.'
            });
        });
};

let createTimetableEntries = (_) => {
    const days = ['Pon', 'Tor', 'Sre', 'Cet', 'Pet'];
    const hours = [
        '07:00',
        '08:00',
        '09:00',
        '10:00',
        '11:00',
        '12:00',
        '13:00',
        '14:00',
        '15:00',
        '16:00',
        '17:00',
        '18:00',
        '19:00',
        '20:00',
        '21:00'
    ];
    const promiseArray = [];

    for (let i = 0; i < days.length; i++) {
        let timetablePromise = new Promise((resolve, reject) => {
            let entry = new Timetable();
            entry.day = days[i];

            for (let j = 0; j < hours.length; j++) {
                let tableslot = new Tableslot();
                tableslot.course = ' ';
                tableslot.classroom = ' ';
                tableslot.duration = 0;
                tableslot.time = hours[j];

                entry.table.push(tableslot);
            }

            entry
                .save()
                .then((dbRes) => {
                    resolve(dbRes);
                })
                .catch((err) => {
                    console.log(err);
                    reject();
                });
        });

        promiseArray.push(timetablePromise);
    }

    return Promise.all(promiseArray);
};

module.exports.all = (req, res) => {
    User.find({}).populate('dataCollection').then((results) => {
        res.status(200).send({
            data: results
        });
    });
};