const fs = require('fs');
const path = __dirname + '/../../parsers/studentFood.json';
const boni = JSON.parse(fs.readFileSync(path));
let https = require('https');
let index = 0;

module.exports.getAll = (req, res) => {
    res.status(200).send({
        b: boni.b
    });
};

function createPromise(i) {
    return new Promise((resolve, reject) => {
        const url =
            'https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=' +
            encodeURIComponent(boni[i].title) +
            '&inputtype=textquery&fields=photos,formatted_address,name,rating,opening_hours,geometry,place_id&key=AIzaSyBFpco1Si1VVnT93GNcoUiYnQroAN1FsmI';

        sendRequest(url, resolve, reject, i, 0);
    }).then((data) => {
        return new Promise((resolve, reject) => {
            var url =
                'https://maps.googleapis.com/maps/api/geocode/json?address=' +
                encodeURIComponent(boni[i].address) +
                '&key=AIzaSyBFpco1Si1VVnT93GNcoUiYnQroAN1FsmI';

            sendRequest(url, resolve, reject, i, 1);
        });
    });
}

function sendRequest(url, resolve, reject, i, requestType) {
    https
        .get(url, (serverRes) => {
            let data = '';

            serverRes.on('data', (chunk) => {
                data += chunk;
            });

            serverRes.on('end', () => {
                const result = JSON.parse(data);
                if (result.status !== 'OK') {
                    return resolve();
                }

                if (requestType === 0) {
                    boni[i].lat = result.candidates[0].geometry.location.lat;
                    boni[i].lng = result.candidates[0].geometry.location.lng;
                    boni[i].rating = result.candidates[0].rating;
                } else {
                    boni[i].lat = result.results[0].geometry.location.lat;
                    boni[i].lng = result.results[0].geometry.location.lng;
                }

                resolve();
            });
        })
        .on('error', (serverErr) => {
            reject(serverErr);
        });
}

function getExtraInfo() {
    const promiseArr = [];
    for (let i = 0; i < boni.length; i++) {
        promiseArr.push(createPromise(i));
    }

    Promise.all(promiseArr)
        .then((results) => {
            console.log('Fetching done.');
        })
        .catch((errorRes) => {
            console.log(errorRes);
        });
}