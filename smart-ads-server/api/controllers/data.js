const mongoose = require('mongoose');
const User = mongoose.model('User');
const Data = mongoose.model('Data');

module.exports.addNewData = (req, res) => {
    let returnId;
    let currentUser;

    User.findOne({ _id: req.userId })
        .then((user) => {
            if (user == null) {
                res.status(400).send({
                    m: 'Uporabnik ne obstaja.'
                });
            } else {
                currentUser = user;
                let data = new Data();
                data.n = req.body.name;
                data.c = req.body.category;
                data.d = req.body.duration;
                data.a = {
                    n: req.body.activity.name,
                    c: req.body.activity.confidence
                };
                data.cn = req.body.connection;
                data.b = {
                    l: req.body.battery.level,
                    iP: req.body.battery.isPlugged
                };
                data.br = req.body.brightness;
                data.t = req.body.time;
                data.l = req.body.location;

                data
                    .save()
                    .then((nModified) => {
                        returnId = nModified._id;
                        currentUser.dataCollection.push(returnId);
                        currentUser.save().then(() => {
                            res.status(200).send({
                                id: returnId
                            });
                        });
                    })
                    .catch((err) => {
                        console.log(err);
                        res.status(500).send({
                            m: 'Prišlo je do napake na strežniku.'
                        });
                    });
            }
        })
        .catch((err) => {
            console.log(err);
            res.status(500).send({
                m: 'Prišlo je do napake na strežniku.'
            });
        });
};

module.exports.all = (req, res) => {
    Data.find({}).then((all) => {
        res.status(200).send({
            all
        });
    });
};

module.exports.addFeedback = (req, res) => {
    const feedbackId = req.body.id;
    User.findOne({ _id: req.userId, dataCollection: { $in: [feedbackId] } }, { 'dataCollection.$': 1 })
        .then((user) => {
            if (user == null) {
                res.status(400).send({
                    m: 'Uporabnik ne obstaja.'
                });
            } else {
                if (user.dataCollection != null && user.dataCollection.length > 0) {
                    Data.updateOne({ _id: feedbackId }, { 'f.a1': req.body.a1, 'f.a2': req.body.a2, 'f.a3': req.body.a3 }).then((nModified) => {
                        res.status(200).send({
                            m: 'OK'
                        });
                    });
                } else {
                    res.status(500).send({
                        m: 'Prišlo je do napake na strežniku.'
                    });
                }
            }
        })
        .catch((err) => {
            console.log(err);
            res.status(500).send({
                m: 'Prišlo je do napake na strežniku.'
            });
        });
};

module.exports.updateLocation = (req, res) => {
    User.findOne({ _id: req.userId })
        .then((user) => {
            if (user == null) {
                res.status(400).send({
                    m: 'Uporabnik ne obstaja.'
                });
            } else {
                updateAllLocations(req.body.data, user.dataCollection)
                    .then(() => {
                        res.status(200).send({
                            m: 'OK'
                        });
                    })
                    .catch((err) => {
                        console.log(err);
                        res.status(500).send({
                            m: 'Prišlo je do napake na strežniku.'
                        });
                    });
            }
        })
        .catch((err) => {
            console.log(err);
            res.status(500).send({
                m: 'Prišlo je do napake na strežniku.'
            });
        });
};

let updateAllLocations = (data, dataCollection) => {
    let array = [];

    for (let i = 0; i < data.length; i++) {
        if (dataCollection.indexOf(data[i].id) != -1) {
            array.push(returnLocationPromise(data[i].id, data[i].location));
        }
    }

    return Promise.all(array);
};

let returnLocationPromise = (id, location) => {
    return new Promise((resolve, reject) => {
        Data.updateOne({ _id: id }, { l: location })
            .then(() => {
                resolve();
            })
            .catch(() => {
                reject();
            });
    });
};