const fs = require('fs');
const path = __dirname + '/../../parsers';
let trainStationIn = JSON.parse(fs.readFileSync(`${path}/trainStationIn.json`));
let trainStationOut = JSON.parse(fs.readFileSync(`${path}/trainStationOut.json`));

let request = require('request');

module.exports.getAllStationsIn = (_, res) => {
    res.status(200).send({ t: trainStationIn });
};

module.exports.getAllStationsOut = (_, res) => {
    res.status(200).send({ t: trainStationOut });
};