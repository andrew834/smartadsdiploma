const mongoose = require('mongoose');
const User = mongoose.model('User');
const Task = mongoose.model('Task');

module.exports.createTask = (req, res) => {
    const userId = req.userId;

    let newTask = new Task();
    newTask._id = mongoose.Types.ObjectId();
    newTask.title = req.body.title;
    newTask.description = req.body.description;
    newTask.urgency = req.body.urgency;
    newTask.date = new Date();

    newTask
        .save()
        .then((dbResponse) => {
            if (dbResponse == null) {
                throw `Uporabnik ${userId} ni uspešno kreiral opravka, saj ni bilo možno ustvariri vnosa v bazo.`;
            }
            return User.updateOne({ _id: userId }, { $push: { t: newTask._id } });
        })
        .then((modified) => {
            if (modified.nModified === 0) {
                throw `Uporabnik ${userId} ni uspešno kreiral opravka.`;
            }

            res.status(200).send({
                d: {
                    title: newTask.title,
                    description: newTask.description,
                    urgency: newTask.urgency,
                    date: newTask.date,
                    _id: newTask._id
                }
            });
        })
        .catch((err) => {
            console.log(err);
            res.status(500).send({
                m: 'Prišlo je do napake na strežniku.'
            });
        });
};

module.exports.getMyTasks = (req, res) => {
    const userId = req.userId;

    User.findOne({ _id: userId }, { t: 1 })
        .populate('t')
        .then((tasks) => {
            if (tasks == null) {
                throw `Prišlo je do kritične napake za uporabnika ${userId}. Token veljaven, a uporabnika ni v bazi.`;
            }

            res.status(200).send({
                t: tasks.t
            });
        })
        .catch((err) => {
            console.log(err);
            res.status(500).send({
                m: 'Prišlo je do napake na strežniku.'
            });
        });
};

module.exports.getSpecificTask = (req, res) => {
    const taskId = req.params.taskId;
    const userId = req.userId;

    User.findOne({
            _id: userId,
            t: { $in: [mongoose.Types.ObjectId(taskId)] }
        }, { 't.$': 1 })
        .then((dbData) => {
            res.status(200).send({
                t: dbData
            });
        })
        .catch((err) => {
            console.log(err);
            res.status(500).send({
                m: 'Prišlo je do napake na strežniku.'
            });
        });
};

module.exports.deleteSpecificTask = (req, res) => {
    const taskId = req.params.taskId;
    const userId = req.userId;

    User.updateOne({
            _id: userId
        }, {
            $pull: {
                t: { $in: [mongoose.Types.ObjectId(taskId)] }
            }
        })
        .then((modified) => {
            if (modified.nModified === 0) {
                throw `Uporabnik ${userId} ni uspešno odstranil opravka.`;
            }

            return Task.deleteOne({ _id: mongoose.Types.ObjectId(taskId) });
        })
        .then((modified) => {
            if (modified.deletedCount == 0) {
                throw `Uporabnik ${userId} ni uspešno odstranil opravka, ker ga ni v Task collectionu.`;
            }

            res.status(200).send({ m: 'Uspeh.' });
        })
        .catch((err) => {
            console.log(err);
            res.status(500).send({
                m: 'Prišlo je do napake na strežniku.'
            });
        });
};

module.exports.updateSpecificTask = (req, res) => {
    const taskId = req.params.taskId;
    const userId = req.userId;

    User.findOne({
            _id: userId,
            t: { $in: [mongoose.Types.ObjectId(taskId)] }
        }, { 't.$': 1 })
        .then((dbData) => {
            if (dbData == null || dbData.t.length == 0) {
                throw `Uporabnik ${userId} ni uspešno posodobil opravka ${taskId}, ker ga nima.`;
            }

            return Task.updateOne({
                _id: mongoose.Types.ObjectId(taskId)
            }, {
                title: req.body.title,
                description: req.body.description,
                urgency: req.body.urgency
            });
        })
        .then((modified) => {
            if (modified.nModified === 0) {
                throw `Uporabnik ${userId} ni uspešno posodobil opravka.`;
            }

            res.status(200).send({ m: 'Uspeh.' });
        })
        .catch((err) => {
            console.log(err);
            res.status(500).send({
                m: 'Prišlo je do napake na strežniku.'
            });
        });
};