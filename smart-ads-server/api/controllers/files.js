const fs = require('fs');
const path = require('path');
const feedbackPath = __dirname + '/feedback.json';
const adsPath = __dirname + '/ads.json';

const feedback = JSON.parse(fs.readFileSync(feedbackPath));
const ads = JSON.parse(fs.readFileSync(adsPath));

module.exports.sendRandomImage = (req, res) => {
    sendFilePath(res, randomFile('image'), 'images/current_batch');
};

module.exports.sendRandomVideoShort = (req, res) => {
    sendFilePath(res, randomFile('video', 'short'), 'videos/current_batch/short');
};

module.exports.sendRandomVideoLong = (req, res) => {
    sendFilePath(res, randomFile('video', 'long'), 'videos/current_batch/long');
};

let randomFile = (type, videoOpt = null) => {
    if (type === 'image') {
        return ads.images[generateRandomIndex(ads.images.length)];
    } else if (type === 'video') {
        if (videoOpt === 'short') {
            return ads.videos['short'][generateRandomIndex(ads.videos['short'].length)];
        } else if (videoOpt === 'long') {
            return ads.videos['long'][generateRandomIndex(ads.videos['long'].length)];
        }
    }
};

let generateRandomIndex = (upperLimit) => {
    return Math.floor(Math.random() * upperLimit);
};

let sendFilePath = (res, fileObj, fileType) => {
    let survey = {
        n: fileObj.name,
        q1: {
            as: [],
            a: fileObj.feedback.a1
        },
        q2: {
            as: [],
            a: fileObj.feedback.a2
        }
    };

    console.log(survey);
    console.log(survey.n);

    let selected = generateRandomSurvey(survey.q1.a, survey.q2.a);
    survey.q1.as = shuffle(selected.as1);
    survey.q2.as = shuffle(selected.as2);

    res.status(200).send({ url: `/${fileType}/${fileObj.name}`, survey });
};

let shuffle = (a) => {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
};

let generateRandomSurvey = (correctAnswer1, correctAnswer2) => {
    let answers1 = [correctAnswer1];
    let answers2 = [correctAnswer2];

    while (answers1.length < 5) {
        const index = generateRandomIndex(feedback.products.length);
        if (answers1.indexOf(feedback.products[index]) == -1) {
            answers1.push(feedback.products[index]);
        }
    }

    while (answers2.length < 5) {
        const index = generateRandomIndex(feedback.firms.length);
        if (answers2.indexOf(feedback.firms[index]) == -1) {
            answers2.push(feedback.firms[index]);
        }
    }

    return {
        as1: answers1,
        as2: answers2
    };
};