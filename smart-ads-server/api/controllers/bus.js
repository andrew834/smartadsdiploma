const fs = require('fs');
const path = __dirname + '/../../parsers';
let toCenterArr = JSON.parse(fs.readFileSync(`${path}/toCenter.json`));
let fromCenterArr = JSON.parse(fs.readFileSync(`${path}/fromCenter.json`));

let request = require('request');

module.exports.getSpecificStationInformation = (req, res) => {
    request(`https://www.lpp.si/lpp/ajax/1/${req.params.stationId}`, (serverErr, serverRes, serverResBody) => {
        if (serverErr) {
            return res.status(500).send({ m: 'Prišlo je do napake.' });
        }

        if (serverRes && serverRes.statusCode === 200) {
            let responseObj = JSON.parse(serverResBody);
            return res.status(200).send({ l: responseObj });
        } else {
            return res.status(500).send({ m: 'Prišlo je do napake.' });
        }
    });
};

module.exports.getAllStationsToCenter = (_, res) => {
    res.status(200).send({ l: toCenterArr });
};

module.exports.getAllStationsFromCenter = (_, res) => {
    res.status(200).send({ l: fromCenterArr });
};