const mongoose = require('mongoose');

let dataSchema = new mongoose.Schema({
    n: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    c: {
        // category
        type: mongoose.Schema.Types.String,
        required: true,
        enum: ['image', 'vshort', 'vlong']
    },
    d: {
        // duration
        type: mongoose.Schema.Types.Number,
        required: true,
        min: 0
    },
    a: {
        // activity
        n: {
            // name
            type: mongoose.Schema.Types.String,
            required: true,
            enum: ['STILL', 'ON_FOOT', 'IN_VEHICLE']
        },
        c: {
            // confidence
            type: mongoose.Schema.Types.Number,
            required: true,
            min: 0,
            max: 100
        }
    },
    cn: {
        // connection
        type: mongoose.Schema.Types.String,
        required: true,
        maxlength: 15
    },
    b: {
        // battery
        l: {
            // level
            type: mongoose.Schema.Types.Number,
            required: true,
            min: 0,
            max: 1
        },
        iP: {
            // isPlugged
            type: mongoose.Schema.Types.Boolean,
            required: true
        }
    },
    br: {
        // brightness
        type: mongoose.Schema.Types.Number,
        required: true,
        min: 0,
        max: 255
    },
    t: {
        // time
        type: mongoose.Schema.Types.Date,
        required: true
    },
    l: {
        // location
        type: mongoose.Schema.Types.String,
        required: true,
        enum: ['HOME', 'WORK', 'OTHER', 'UNKNOWN']
    },
    f: {
        // feedback
        a1: {
            type: mongoose.Schema.Types.String,
            required: false
        },
        a2: {
            type: mongoose.Schema.Types.String,
            required: false
        },
        a3: {
            type: mongoose.Schema.Types.Number,
            required: false,
            min: -2,
            max: 2
        }
    }
});

mongoose.model('Data', dataSchema);