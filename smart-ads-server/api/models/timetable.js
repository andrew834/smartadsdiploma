const mongoose = require('mongoose');

let tableSchema = new mongoose.Schema({
    _id: false,
    course: {
        type: mongoose.Schema.Types.String,
        required: true,
        minlength: 1,
        maxlength: 7
    },
    classroom: {
        type: mongoose.Schema.Types.String,
        required: true,
        minlength: 1,
        maxlength: 7
    },
    duration: {
        type: mongoose.Schema.Types.Number,
        required: true,
        min: 0,
        max: 5,
        default: 0
    },
    time: {
        type: mongoose.Schema.Types.String,
        required: true,
        enum: [
            '07:00',
            '08:00',
            '09:00',
            '10:00',
            '11:00',
            '12:00',
            '13:00',
            '14:00',
            '15:00',
            '16:00',
            '17:00',
            '18:00',
            '19:00',
            '20:00',
            '21:00'
        ]
    }
});

let timetableSchema = new mongoose.Schema({
    day: {
        type: mongoose.Schema.Types.String,
        required: true,
        enum: ['Pon', 'Tor', 'Sre', 'Cet', 'Pet']
    },
    table: {
        type: [tableSchema],
        required: true
    }
});

mongoose.model('Tableslot', tableSchema);
mongoose.model('Timetable', timetableSchema);