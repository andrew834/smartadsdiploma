const mongoose = require('mongoose');

require('./timetable');
require('./users');
require('./tasks');
require('./data');

const isWin = process.platform === 'win32';

let dbURI = 'mongodb://mongodb:27017/smartads?authSource=admin';

const options = {
    useNewUrlParser: true,
    useCreateIndex: true
};

console.log('Platform is Windows: ' + isWin);

if (isWin) {
    dbURI = 'mongodb://192.168.99.100/smartads?authSource=admin';
}

if (process.env.DB_URI) {
    dbURI = process.env.DB_URI;
}

mongoose.connect(dbURI, options);

mongoose.connection.on('connected', (_) => {
    console.log('Mongoose is connected on: ' + dbURI);
});

mongoose.connection.on('error', (err) => {
    console.log('Mongoose encounterd an error when connecting: ' + err);
});

mongoose.connection.on('disconnected', (_) => {
    console.log('Mongoose has closed the connection.');
});

let closeMongoose = (msg, cb) => {
    mongoose.connection.close((_) => {
        console.log('Mongoose closed the connection via ' + msg);
        cb();
    });
};

process.once('SIGUSR2', (_) => {
    closeMongoose('Nodemon reboot.', (_) => {
        process.kill(process.pid, 'SIGUSR2');
    });
});

process.on('SIGINT', (_) => {
    closeMongoose('Leaving the app.', (_) => {
        process.exit(0);
    });
});

process.on('SIGTERM', (_) => {
    closeMongoose('Leaving the app on Heroku.', (_) => {
        process.exit(0);
    });
});