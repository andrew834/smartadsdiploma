const mongoose = require('mongoose');
const crypto = require('crypto');
const config = require('../config/config.js');
const jwt = require('jsonwebtoken');

let userSchema = new mongoose.Schema({
    u: {
        type: mongoose.Schema.Types.String,
        unique: true,
        required: true,
        dropDups: true,
        minlength: 3,
        maxlength: 20
    },
    p: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    b: {
        type: mongoose.Schema.Types.Date,
        required: true
    },
    g: {
        type: mongoose.Schema.Types.String,
        enum: ['m', 'f', 'o'],
        required: true
    },
    rV: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    t: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Task'
    }],
    timetable: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Timetable'
    }],
    dataCollection: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Data'
    }]
});

userSchema.methods.setPassword = function(pass) {
    this.rV = crypto.randomBytes(16).toString('hex');
    this.p = crypto.pbkdf2Sync(pass, this.rV, 2000, 64, 'sha256').toString('hex');
};

userSchema.methods.checkPassword = function(pass) {
    let passToCompare = crypto.pbkdf2Sync(pass, this.rV, 2000, 64, 'sha256').toString('hex');
    return this.p === passToCompare;
};

userSchema.methods.generateToken = function() {
    return jwt.sign({
            id: this._id,
            u: this.u,
            b: this.b,
            g: this.g
        },
        config.s, {
            expiresIn: 60 * 60 * 12 // The token is valid for 12h.
        }
    );
};

mongoose.model('User', userSchema);