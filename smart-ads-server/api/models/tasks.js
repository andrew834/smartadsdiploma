const mongoose = require('mongoose');

let taskSchema = new mongoose.Schema({
    title: {
        type: mongoose.Schema.Types.String,
        required: true,
        minlength: 3,
        maxlength: 40
    },
    description: {
        type: mongoose.Schema.Types.String,
        required: true,
        maxlength: 2000
    },
    urgency: {
        required: true,
        type: mongoose.Schema.Types.String,
        enum: ['#AFE067', '#FFCF68', '#4C8DFF', '#F25454']
    },
    date: {
        required: true,
        type: mongoose.Schema.Types.Date,
        default: new Date()
    }
});

mongoose.model('Task', taskSchema);