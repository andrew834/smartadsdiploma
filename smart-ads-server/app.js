const express = require('express');
const logger = require('morgan');
const helmet = require('helmet');
const instanceId = require('uniqid')('smartads', 'server');

// Ddos protection
const reqLimit = require('express-rate-limit');
const limiter = reqLimit({
    windowMs: 5 * 60 * 1000,
    max: 500,
    message: 'You have reached the maximum number of requests. Please try again later.'
});
const fileLimiter = reqLimit({
    windowMs: 5 * 60 * 1000,
    max: 50,
    message: 'You have reached the maximum number of requests for files. Please try again later.'
});

// Accessing the Database
require('./api/models/db');

const usersApi = require('./api/routes/users');
const fileApi = require('./api/routes/files');
const boniApi = require('./api/routes/boni');
const busApi = require('./api/routes/bus');
const taskApi = require('./api/routes/tasks');
const trainApi = require('./api/routes/train');
const timetableApi = require('./api/routes/timetable');
const dataApi = require('./api/routes/data');

const app = express();

app.use(express.static('public'));

app.enable('trust proxy');

app.use(helmet());
app.use(
    helmet.contentSecurityPolicy({
        directives: {
            defaultSrc: ["'self'"]
        }
    })
);

app.use((_, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', '*');
    res.header;
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, X-Access-Token');
    next();
});

app.use(logger('dev'));
app.use(express.json({ limit: '60kb' }));
app.use(
    express.urlencoded({
        limit: '60kb',
        extended: true
    })
);

app.use((_, res, next) => {
    res.charset = 'UTF-8';
    next();
});

// Routes to access the Api
app.use('/user', limiter, usersApi);
app.use('/file', fileLimiter, fileApi);
app.use('/boni', limiter, boniApi);
app.use('/bus', limiter, busApi);
app.use('/task', limiter, taskApi);
app.use('/train', limiter, trainApi);
app.use('/timetable', limiter, timetableApi);
app.use('/data', limiter, dataApi);

// Error handling
app.use((_, res) => {
    res.status(404).send({
        m: 'Prazen zahtevek.',
        id: instanceId
    });
});

// Error handling
app.use((err, req, res, next) => {
    console.log(err);
    res.status(404).send({
        m: 'Prazen zahtevek.',
        id: instanceId
    });
});

module.exports = app;